from django.http import HttpResponseBadRequest
from django.shortcuts import render, redirect

from .settings import DEBUG


def index(request):
    if DEBUG:
        return redirect('home')
    else:
        return HttpResponseBadRequest()
        return redirect('https://qahacking.ru/sajty')


def actual_index(request):
    return render(request, 'index.html')
