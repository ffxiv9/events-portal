"""integration_service URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import debug_toolbar
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

from .views import index, actual_index

urlpatterns = [
    path('', index, name='wrong_home'),
    path('you+shall+not+pass/', actual_index, name='home'),
    path('you+shall+not+pass/user/', include('users.form_urls')),
    path('you+shall+not+pass/skill/', include('skills.form_urls')),
    path('admin/', admin.site.urls),
    path('__debug__/', include(debug_toolbar.urls)),
    path('auth/', include('auth.urls')),
    path('api/', include('users.api_urls')),
    path('api/image/', include('images.urls')),
    path('api/locations/', include('locations.urls')),
    path('api/tags/', include('skills.api_urls')),

]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
