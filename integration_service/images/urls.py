from django.urls import path

from . import views

app_name = 'images'

urlpatterns = [
    path('<str:filename>/', views.ImageView.as_view(), name='create-image'),
    path('<str:filename>/<int:width>x<int:height>/', views.ImageView.as_view(), name='create-image'),
    path('', views.JsonImageView.as_view(), name='create-image'),
]
