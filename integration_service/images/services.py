from io import BytesIO
from pathlib import Path

from PIL import Image as Pillow
from django.core.exceptions import ValidationError as DjangoValidationError
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage
from django.shortcuts import get_object_or_404
from rest_framework.exceptions import ParseError
from rest_framework.exceptions import ValidationError

from .models import Image


def create_image(content_file, name):
    verify_image(content_file)
    verify_name(name)
    try:
        new_image = Image()
        new_image.name = name
        new_image.validate_unique()
        new_image.file.save(content_file.name, content_file.file, save=False)
        new_image.save()
    except DjangoValidationError as exc:
        raise ValidationError(exc.message_dict)


def verify_name(name):
    name_max_len = Image._meta.get_field('name').max_length
    if len(name) > name_max_len:
        raise ValidationError(f"name too long. name_max_len = {name_max_len}")


def verify_image(file_obj):
    try:
        img = Pillow.open(file_obj.file)
        img.verify()
    except Exception:
        raise ParseError("Unsupported image type")


def get_image_url_or_create(filename, width=None, height=None):
    image = get_object_or_404(Image, name=filename)

    if not width and not height:
        return image.file.url

    new_width, new_height = get_new_size(width, height, image)
    new_path = add_name_suffix(image.file.name, f'_{new_width}x{new_height}')

    if not default_storage.exists(new_path):
        create_resized_image(image.file.file, new_width, new_height, new_path)

    return default_storage.url(new_path)


def create_resized_image(file, width, height, saving_path):
    pillow_img = Pillow.open(file)
    resized_img = pillow_img.resize((width, height), Pillow.ANTIALIAS)
    buffer = BytesIO()
    resized_img.save(buffer, format=pillow_img.format)
    default_storage.save(saving_path, ContentFile(buffer.getvalue()))


def add_name_suffix(filename, name_suffix):
    path = Path(filename)
    new_name = f'{path.stem}{name_suffix}{path.suffix}'
    return filename.replace(path.name, new_name)


def get_new_size(required_width, required_height, image):
    required_ratio = required_width / required_height
    present_ratio = image.width / image.height
    if present_ratio > required_ratio:
        new_height = required_height
        new_width = int(new_height * image.width / image.height)
    else:
        new_width = required_width
        new_height = int(new_width * image.height / image.width)

    return new_width, new_height
