import base64
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage


def parse_data_uri_file(data_uri, filename):
    data_format, base64_str = data_uri.split(';base64,')
    ext = data_format.split('/')[-1]
    if ext[:3] == 'svg':
        ext = 'svg'
    if not filename.endswith(ext):
        filename = f'{filename}.{ext}'
    filename = default_storage.get_valid_name(filename)

    return ContentFile(base64.b64decode(base64_str), name=filename)
