from django.db import models


class Image(models.Model):
    name = models.CharField(max_length=256, unique=True)
    width = models.IntegerField()
    height = models.IntegerField()
    file = models.ImageField(upload_to='events_images/%Y/%m/',
                             width_field='width', height_field='height')

    def __str__(self):
        return self.name
