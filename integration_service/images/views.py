from django.http import HttpResponse
from django.shortcuts import redirect
from rest_framework.exceptions import ParseError
from rest_framework.parsers import FileUploadParser, JSONParser
from rest_framework.views import APIView

from .data_uri import parse_data_uri_file
from .services import create_image, get_image_url_or_create


class ImageUploadParser(FileUploadParser):
    media_type = 'image/*'


class ImageView(APIView):
    parser_classes = [ImageUploadParser]

    def post(self, request, filename):
        if 'file' not in request.data:
            raise ParseError("Empty content")

        content_file = request.data['file']
        create_image(content_file, filename)
        return HttpResponse(status=201)

    def get(self, request, filename, width=None, height=None):
        return redirect(get_image_url_or_create(filename, width, height))


class JsonImageView(APIView):
    parser_classes = [JSONParser]

    def post(self, request):
        name = request.data.get("name", None)
        base64_file = request.data.get("file", None)

        if not name or not base64_file:
            raise ParseError('Two fields is require: "name" and "file"')

        content_file = parse_data_uri_file(base64_file, name)
        create_image(content_file, name)
        return HttpResponse(status=201)
