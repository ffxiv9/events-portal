from django.urls import path

from . import views

app_name = 'api_users'

urlpatterns = [
    path('user_info/', views.get_user_info_by_id,
         name='user-info-by-id'),
    path('user/<str:username>/', views.get_user_info_by_username,
         name='user-info-by-username'),
    path('user/', views.create_user, name='create-user'),
]
