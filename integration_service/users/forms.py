from django import forms

from .models import User


class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('username', 'password', 'first_name', 'last_name', 'email',
                  'location', 'position', 'skills')

    username = forms.CharField(min_length=9, max_length=35, required=True)
    password = forms.CharField(min_length=6, max_length=30, required=True,
                               widget=forms.PasswordInput())

    def create_user(self):
        new_user = self.save(commit=False)
        new_user.set_password(self.data['password'])
        new_user.save()
        return new_user
