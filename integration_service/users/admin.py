from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

from .models import User


class UserAdmin(BaseUserAdmin):
    fieldsets = (
        *BaseUserAdmin.fieldsets,
        ('Additional information', {'fields': ('location', 'position', 'skills')})
    )
    raw_id_fields = ['skills']


admin.site.register(User, UserAdmin)
