from django.http import JsonResponse, HttpResponse, HttpResponseBadRequest
from django.shortcuts import get_object_or_404, render
from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView
from django.views.generic.edit import FormView
from django.views.generic.edit import UpdateView, DeleteView
from rest_framework.decorators import api_view
from rest_framework.exceptions import ValidationError

from .forms import UserForm
from .models import User
from .serializers import serialize_user


class UserListView(ListView):
    model = User
    template_name = 'users/list.html'
    queryset = User.objects.all().order_by('id')


class UserDetailView(DetailView):
    model = User
    template_name = 'users/detail.html'


class UserUpdateView(UpdateView):
    model = User
    template_name = 'users/edit.html'
    form_class = UserForm

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        del form.fields['password']
        return form


class UserDeleteView(DeleteView):
    model = User
    template_name = 'users/delete.html'
    success_url = reverse_lazy('form_users:list')


class UserCreateView(FormView):
    template_name = 'users/create.html'
    form_class = UserForm

    def form_valid(self, user_form):
        new_user = user_form.create_user()
        return render(self.request, 'users/created.html',
                      context={'user': new_user})


@api_view(['GET'])
def get_user_info_by_username(request, username):
    user = get_object_or_404(User, username=username)
    return JsonResponse(serialize_user(user))


@api_view(['GET'])
def get_user_info_by_id(request):
    try:
        user_id = int(request.query_params['StaffId'])
    except KeyError:
        return HttpResponseBadRequest('StaffId is required.')

    user = get_object_or_404(User, id=user_id)
    return JsonResponse(serialize_user(user))


@api_view(['POST'])
def create_user(request):
    user_form = UserForm(request.data)
    if not user_form.is_valid():
        raise ValidationError(user_form.errors)

    user_form.create_user()
    return HttpResponse(status=201)
