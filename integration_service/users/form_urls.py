from django.urls import path

from . import views

app_name = 'form_users'

urlpatterns = [
    path('create/', views.UserCreateView.as_view(), name='create'),
    path('', views.UserListView.as_view(), name='list'),
    path('<int:pk>/', views.UserDetailView.as_view(), name='detail'),
    path('<int:pk>/edit/', views.UserUpdateView.as_view(), name='edit'),
    path('<int:pk>/delete/', views.UserDeleteView.as_view(), name='delete'),
]
