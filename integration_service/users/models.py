from django.contrib.auth.models import AbstractUser
from django.core import validators
from django.db import models
from django.urls import reverse
from django.utils.deconstruct import deconstructible

from skills.models import Skill


@deconstructible
class LocationValidator(validators.RegexValidator):
    regex = r'^[A-Z][A-Za-z]+(\s[A-Za-z]+)*, [A-Z][A-Za-z]*(\s[A-Za-z]+)*$'
    message = 'Enter a valid location. ' \
              'This value may contain only letters and one comma. ' \
              'Format: "City, Country". First letter must be uppercase.'
    flags = 0


class User(AbstractUser):
    location = models.CharField(
        max_length=100,
        help_text='Required. 100 characters or fewer. '
                  'Format: "City, Country".',
        validators=[LocationValidator()])

    position = models.CharField(max_length=150)
    skills = models.ManyToManyField(Skill, related_name="users", blank=True)

    def get_absolute_url(self):
        return reverse('form_users:detail', args=[str(self.id)])
