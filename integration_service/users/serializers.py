def serialize_user(user):
    data = {
        'staff_id': user.id,
        'username': user.username,
        'first_name': user.first_name,
        'last_name': user.last_name,
        'email': user.email,
        'location': user.location,
        'position': user.position,
    }
    return data
