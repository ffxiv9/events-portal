from django.http import JsonResponse
from rest_framework.decorators import api_view

from .services import get_all_locations


@api_view(['GET'])
def get_locations(request):
    return JsonResponse(get_all_locations(), safe=False)
