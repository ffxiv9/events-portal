from users.models import User


def get_all_locations():
    locations_set = User.objects.values('location').distinct().exclude(
        location='')
    return [item['location'] for item in locations_set]
