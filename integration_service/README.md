# API Service for integration

## Installation

You need Python 3 to run it.

You need PostgreSQL installed. 

Create empty database with the name "integration_service".

Download code from GitHub. 

Install dependencies:

```shell script
pip install -r requirements.txt
```

If you want you can install dependencies to virtual environment. 
To create and activate virtual environment you should run:
```shell script
python -m venv venv
venv\Scripts\activate
```

Set the following environment variables:
 - Django settings:
   - `SECRET_KEY` - A secret key for a particular Django installation. 
   This is used to provide cryptographic signing, 
 and should be set to a unique, unpredictable value. For example:  
 `*2$jn28iv=-7x$u-dh4ttv^fd8vrxiba5xe2w#5=c9o(627$&q`
   - `DEBUG` - A boolean that turns on/off debug mode. For example: `True`
 - JWT settings:
   - `JWT_SECRET_KEY` - A secret key to use for signing the JWT tokens. 
   For example: 
 `XbPfbKIURF6arZ3Y922BhjWgQzWXcXNrz0ogtVhfEd2o`
   - `ISSUER_FOR_JWT` - Identifies principal that issued the JWT. 
   For example: `127.0.0.1:8000`
   - `AUDIENCE_FOR_JWT` - Identifies the recipient that the JWT is 
   intended for. For example: `localhost:5001`
 - PostgeSQL database settings:
   - `PG_USER` - User name. For example: `postgres`
   - `PG_PASSWORD` - Password. For example: `postgres`
   - `PG_HOST` - Host. For example: `localhost`
   - `PG_PORT` - Port. For example: `5432`

You can use file `.env-example` for it. Just rename it to `.env` and change 
some values if ypu need.

Update database schema:

```shell script
python3 manage.py migrate
```

Run server:

```shell script
python3 manage.py runserver
```

The service will be at [127.0.0.1:8000](http://127.0.0.1:8000)

## Usage

### API description

#### Authentication:

 - `POST /auth/token/` - generate and returns JWT for username with password.
   - Request:
     ```http request
     POST /auth/token/ HTTP/1.1
     Host: 127.0.0.1:8000
     Content-Type: application/json
    
     {
       "username": "vasya",
       "password": "P@ssW0rd&"
     }
     ```
   - Response:
     ```json
     {
       "token": "eyJhbGciOiJIybe125qQH4hs4Bc..."
     }
     ```
     
#### Users:
     
 - `GET /api/user/<str:username>/` - returns information about user with 
 username=`username`.
    - Response:
      ```json
      {
          "username": "the.pyetr1", 
          "first_name": "Pyetr", 
          "last_name": "Perviy", 
          "email": "first@petr.ru", 
          "location": "Saint Petersburg", 
          "position": "Imperator"
      }
      ```
 - `POST /api/user/` - create new user. 
   - Example of request:
     ```http request
     POST /api/user/ HTTP/1.1
     Host: 127.0.0.1:8000
     Content-Type: application/json
     
     {
       "username": "the.pyetr1",
       "password": "Window_t0_eur0pe",
       "first_name": "Pyetr",
       "last_name": "Perviy",
       "email": "first@petr.ru",
       "location": "Saint Petersburg",
       "position": "Imperator"
     }
     ```
    - Response will be just: `201 Created`
    
#### Images:

 - `GET /api/image/<str:filename>/` - returns URL to image with 
 filename=`filename` in **original** resolution.
    - Example:
      - Request:
       ```http request
       GET /api/image/2af288fa0ee4/ HTTP/1.1
       Host: 127.0.0.1:8000
       ```
      - Response:
       ```
       http://127.0.0.1:8000/media/events_images/2020/11/2af288fa0ee4.jpeg
       ```
 - `GET /api/image/<str:filename>/<int:width>x<int:height>/` - returns URL 
 to image with filename=`filename` in **specified** resolution.
    - Example:
      - Request:
       ```http request
       GET /api/image/2af288fa0ee4/250x180/ HTTP/1.1
       Host: 127.0.0.1:8000
       ```
      - Response:
       ```
       http://127.0.0.1:8000/media/events_images/2020/11/2af288fa0ee4_250x140.jpeg
       ```
 - `POST /api/image/` - create new image. 
   - Example
     - Request:
      ```http request
      POST /api/image/ HTTP/1.1
      Host: 127.0.0.1:8000
      Content-Type: application/json
     
      {
        "name": "2af288fa0ee4",
        "file": "data:image/jpeg;base64,/9j/2wCEAAEBAQEBAQEBAQ......."
      }
      ```
     - Response will be just: `201 Created`
 - `POST /api/image/<str:filename>/` - create new image. 
   - Example
     - Request:
      ```http request
      POST /api/image/2af288fa0ee4/ HTTP/1.1
      Host: 127.0.0.1:8000
      Content-Type: Image/jpeg
     
      <--Content of File-->
      ```
     - Response will be just: `201 Created`

#### Locations:
 - `GET /api/locations/` - returns list of locations.
    - Example:
      - Request:
       ```http request
       GET /api/locations/ HTTP/1.1
       Host: 127.0.0.1:8000
       ```
      - Response:
       ```json
       {
            "locations": [
                "Odessa, Ukraine",
                "Saint Petersburg, Russia",
                "Lublin, Poland",
                "Voronezh, Russia"
            ]
        }
       ```
