from django.urls import path

from . import views

app_name = 'auth'

urlpatterns = [
    path('token/', views.get_token, name='token'),
]
