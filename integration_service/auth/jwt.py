from datetime import timedelta

from django.conf import settings
from django.utils import timezone
from jose import jwt


def create_token(username, audiences, validity=timedelta(days=1)):
    if not audiences:
        raise ValueError

    if type(audiences) == list:
        for item in audiences:
            if not item:
                raise ValueError

    payload = {
        'iss': settings.ISSUER_FOR_JWT,
        'aud': audiences,
        'username': username,
        'exp': timezone.now() + validity,
    }

    return jwt.encode(payload, settings.JWT_SECRET_KEY, algorithm='HS256')


def decode(token, audience):
    if not audience:
        raise ValueError

    decoded_payload = jwt.decode(token, settings.JWT_SECRET_KEY,
                                 algorithms='HS256', audience=audience)
    return decoded_payload
