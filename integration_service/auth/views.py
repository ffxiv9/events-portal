from datetime import timedelta

from django.conf import settings
from django.http import JsonResponse
from rest_framework.decorators import api_view
from rest_framework.exceptions import APIException, ParseError

from .jwt import create_token
from .services import check_auth_data


class InternalServerError(APIException):
    status_code = 500
    default_detail = 'Internal server error.'
    default_code = 'service_error'


@api_view(['POST'])
def get_token(request):
    username = request.data.get("username", None)
    password = request.data.get("password", None)

    if not username or not password:
        raise ParseError

    check_auth_data(username, password)
    try:
        token = create_token(
            username,
            audiences=[settings.AUDIENCE_FOR_JWT],
            validity=timedelta(days=1)
        )
    except ValueError:
        raise InternalServerError(detail='Token generation error')

    return JsonResponse({
        'token': token,
    })
