from rest_framework.exceptions import NotFound

from users.models import User


def check_auth_data(username, password):
    user = get_user_by_username(username)
    check_password(password, user)


def check_password(password, user):
    if not user.check_password(password):
        raise NotFound(detail='password incorrect')


def get_user_by_username(username):
    try:
        user = User.objects.get(username=username)
    except User.DoesNotExist as exc:
        raise NotFound(detail='username not found') from exc
    return user
