from datetime import timedelta

from django.conf import settings
from django.test import TestCase, override_settings
from freezegun import freeze_time

from .jwt import create_token, decode

ISSUER_FOR_TEST = 'SOME_ISSUER'
AUDIENCE_FOR_TEST = 'SOME_AUDIENCE'
USERNAME_FOR_TEST = 'TestUser'
SECRET_KEY_FOR_TEST = 'SOME_SECRET_KEY'


class JwtTest(TestCase):

    @freeze_time("2020-01-01 02:04:08", tz_offset=3)
    @override_settings(ISSUER_FOR_JWT=ISSUER_FOR_TEST)
    @override_settings(AUDIENCE_FOR_JWT=AUDIENCE_FOR_TEST)
    @override_settings(JWT_SECRET_KEY=SECRET_KEY_FOR_TEST)
    def test_create_token(self):
        token = create_token(
            USERNAME_FOR_TEST,
            audiences=[settings.AUDIENCE_FOR_JWT],
            validity=timedelta(days=1)
        )
        self.assertEqual(
            token,
            'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJTT01FX0lTU1VFUiIs'
            'ImF1ZCI6WyJTT01FX0FVRElFTkNFIl0sInVzZXJuYW1lIjoiVGVzdFVzZXIiLCJle'
            'HAiOjE1Nzc5MzA2NDh9.0ddTBjdjdLdzHE-Pi6CJfOF-Oen7Uwdl_0LJMbrrg30'
        )
        payload = decode(token, audience=settings.AUDIENCE_FOR_JWT)
        expected_payload = {
            'iss': ISSUER_FOR_TEST,
            'aud': [AUDIENCE_FOR_TEST],
            'username': USERNAME_FOR_TEST,
            'exp': 1577930648,
        }
        self.assertEqual(payload, expected_payload)
