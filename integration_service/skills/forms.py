from django import forms
from django.http import Http404

from .models import Skill


def get_parent_choices(parent_id):
    if not parent_id:
        return (None, '/'),
    try:
        parent_skill = Skill.objects.get(id=parent_id)
        return (parent_skill.id, parent_skill.name),
    except Skill.DoesNotExist:
        raise Http404


class SkillForm(forms.ModelForm):
    class Meta:
        model = Skill
        fields = ('name', 'description', 'parent',)
        widgets = {'parent': forms.HiddenInput()}
