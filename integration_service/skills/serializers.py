def serialize_skills(skills):
    return [serialize_skill(skill) for skill in skills]


def serialize_skill(skill):
    return {
        'id': skill.id,
        'name': skill.name,
        'parent': skill.parent_id,
    }
