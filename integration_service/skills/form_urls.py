from django.urls import path

from . import views

app_name = 'form_skills'

urlpatterns = [
    path('', views.SkillView.as_view(), name='list'),
    path('create/', views.SkillCreate.as_view(), name='create'),
    path('<int:parent>/create/', views.SkillCreate.as_view(), name='create'),
    path('<int:pk>/edit/', views.SkillUpdate.as_view(), name='edit'),
    path('<int:pk>/delete/', views.SkillDelete.as_view(), name='delete'),
    path('<int:pk>/', views.SkillDetail.as_view(), name='detail'),
]
