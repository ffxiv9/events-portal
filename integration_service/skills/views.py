from django.http import JsonResponse
from django.shortcuts import redirect
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import View, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from rest_framework.decorators import api_view

from .forms import SkillForm
from .models import Skill
from .services import get_skills, skill_branch, get_children_skills
from .services import get_skills_tree, walk_skills_tree, get_tag_list


@api_view(['GET'])
def get_tags(request):
    return JsonResponse(get_tag_list(), safe=False)


class SkillView(View):
    def get(self, request):
        skills_tree = get_skills_tree()
        return render(request, 'skills/skills.html',
                      context={'skills': walk_skills_tree(skills_tree, 0)})


class SkillCreate(CreateView):
    model = Skill
    template_name = 'skills/skill_create.html'
    form_class = SkillForm

    def get(self, request, parent=None, *args, **kwargs):
        self.initial = {'parent': parent}
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.initial['parent']:
            context.update(
                {'branch': skill_branch(self.initial['parent'], get_skills())})
        return context


class SkillUpdate(UpdateView):
    model = Skill
    template_name = 'skills/skill_edit.html'
    form_class = SkillForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            {'branch': skill_branch(self.object.id, get_skills(), False)})
        return context


class SkillDelete(DeleteView):
    model = Skill
    template_name = 'skills/skill_delete.html'
    success_url = reverse_lazy('form_skills:list')


class SkillDetail(DetailView):
    model = Skill
    template_name = 'skills/skill_detail.html'

    def get(self, request, pk=None, *args, **kwargs):
        if not pk:
            return redirect('form_skills:list')
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            {'branch': skill_branch(self.object.id, get_skills(), False),
             'children': get_children_skills(self.object.id)})
        return context
