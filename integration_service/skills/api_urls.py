from django.urls import path

from . import views

app_name = 'api_skills'

urlpatterns = [
    path('', views.get_tags, name='get-skills'),
]
