from django.db import models
from django.urls import reverse


class Skill(models.Model):
    name = models.CharField(max_length=64, db_index=True)
    description = models.TextField(blank=True)
    parent = models.ForeignKey('self', related_name='children',
                               on_delete=models.CASCADE, blank=True, null=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['name', 'parent'],
                                    name='unique_skill_name')
        ]

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('form_skills:detail', args=[str(self.id)])
