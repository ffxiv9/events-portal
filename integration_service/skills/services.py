from dataclasses import dataclass, field
from typing import Optional
from operator import attrgetter
from .models import Skill


@dataclass
class SkillTree:
    id: int
    name: str
    children: dict = field(default_factory=dict)


@dataclass
class SkillBranch:
    id: int
    name: str
    parent: Optional[int]


def get_tag_list():
    return [skill.name for skill in Skill.objects.all()]


def get_skills_tree():
    skills = get_skills()
    skills_tree = SkillTree(id=0, name='/')
    for skill in skills:
        add_branch_to_tree(skills_tree, skill_branch(skill, skills))
    return skills_tree


def get_skills():
    skills = dict()
    for skill in Skill.objects.all():
        skills[skill.id] = SkillBranch(skill.id, skill.name, skill.parent_id)
    return skills

def get_children_skills(id):
    return Skill.objects.filter(parent=id).order_by('name')

def skill_branch(item_id, items, include_the_current=True):
    item = items[item_id]
    stack = []
    if include_the_current:
        stack.append(item)

    while item.parent:
        item = items[item.parent]
        stack.append(item)

    while stack:
        yield stack.pop()


def add_branch_to_tree(node, branch):
    for item in branch:
        if item.id not in node.children:
            node.children[item.id] = SkillTree(item.id, item.name)
        node = node.children[item.id]


def walk_skills_tree(node, depth):
    for child in sorted(node.children.values(), key=attrgetter('name')):
        yield depth * 2, child
        for i, n in walk_skills_tree(child, depth + 1):
            yield i, n
