#!/bin/sh

if test -f "deployment/azure/flag_to_do_build"; then
  python manage.py collectstatic --no-input
  python manage.py migrate --no-input
  rm deployment/azure/flag_to_do_build
fi

GUNICORN_CMD_ARGS="--timeout 600 --access-logfile '-' --error-logfile '-' --bind=0.0.0.0:8000 --chdir=/home/site/wwwroot" gunicorn integration_service.wsgi
