param (
    [switch] $i = $false
)

$webApp = $PWD.Path + "/EventsPortalWebApp"
$webServices = $PWD.Path + "/EventsPortal.Service/EventsPortal.WebApi"
$integartionalService = $PWD.Path + "/integration_service"

$profileName = "Command Prompt"

$procArgs = @(
    "-p `"$profileName`" -d `"$webApp`""
    "split-pane -p `"$profileName`" -d `"$webServices`""
    "split-pane -H -d `"$integartionalService`""
)

if (!$i) {
    $procArgs = @(
        $procArgs[0] + " ng serve"
        $procArgs[1] + " dotnet run"
        $procArgs[2] + " py manage.py runserver"
    )
}
else {
    $procArgs = @(
        $procArgs[0] + " npm install"
        $procArgs[1] + " dotnet restore"
        $procArgs[2] + " pip install -r requirements.txt"
    )
}

$argList = $procArgs -join " ; "

Start-Process wt -ArgumentList $argList

