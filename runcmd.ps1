param (
    [switch] $i = $false
)

$webApp = $PWD.Path + "/EventsPortalWebApp"
$webServices = $PWD.Path + "/EventsPortal.Service/EventsPortal.WebApi"
$integartionalService = $PWD.Path + "/integration_service"

$profileName = "cmd";
$procArgs = @(
    "/k cd  `"$webApp`""
    "/k cd  `"$webServices`""
    "/k cd  `"$integartionalService`""
)

if(!$i) {
    $procArgs = @(
        $procArgs[0] + " & ng serve"
        $procArgs[1] + " & dotnet run"
        $procArgs[2] + " & py manage.py runserver"
    )
}else{
    $procArgs = @(
        $procArgs[0] + " & npm install"
        $procArgs[1] + " & dotnet restore"
        $procArgs[2] + " & pip install -r requirements.txt"
    )
}

Foreach ($arg in $procArgs)
{
    Start-Process $profileName -ArgumentList ($arg)
}

