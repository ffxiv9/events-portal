﻿namespace EventsPortal.Models
{
    public class AuthenticatedUser
    {
        public int? UserId { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsAuthenticated => UserId.HasValue;
    }
}