﻿using System;

namespace EventsPortal.Models
{
    [Flags]
    public enum EventVenueType
    {
        Offline = 1,
        Online = 2,
        OnlineAndOffline = Offline | Online
    }
}