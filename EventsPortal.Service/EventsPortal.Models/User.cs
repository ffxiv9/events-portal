﻿using System.Collections.Generic;

namespace EventsPortal.Models
{
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Location { get; set; }
        public string Position { get; set; }
        public string Email { get; set; }
        public virtual ICollection<Event> CreatedEvents { get; set; }
        public virtual ICollection<EventUser> EventUsers { get; set; }
    }
}