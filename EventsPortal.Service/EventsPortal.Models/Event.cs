﻿using System;
using System.Collections.Generic;

namespace EventsPortal.Models
{
    public class Event
    {
        public int Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime StartDate { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public EventVenueType VenueType { get; set; }
        public string Image { get; set; }
        public string LinkBroadcast { get; set; }
        public string SpeakerName { get; set; }
        public int Duration { get; set; }
        public string Location { get; set; }
        public int CreatorId { get; set; }
        public virtual User Creator { get; set; }
        public virtual ICollection<Speaker> Speakers { get; set; }
        public virtual ICollection<EventUser> EventUsers { get; set; }
    }
}