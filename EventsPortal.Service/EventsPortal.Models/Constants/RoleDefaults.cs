﻿namespace EventsPortal.Models.Constants
{
    public static class RoleDefaults
    {
        public const string DefaultRole = "User";
        public const string DefaultClaim = "role";
        public const string DefaultAdminRole = "PRManager";
    }
}