﻿namespace EventsPortal.Models.Constants
{
    public static class OutlookConstants
    {
        public const string OnlineEventAlarmInfoAddition =
            "This is Online event. Your link to the translation will be available on event details page. You can check it closer to event start date";
    }
}