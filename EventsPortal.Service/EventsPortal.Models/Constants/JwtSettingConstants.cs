﻿namespace EventsPortal.Models.Constants
{
    public static class JwtSettingConstants
    {
        public const string Audience = "Jwt:Audience";
        public const string Issuer = "Jwt:Issuer";
        public const string SecretKey = "Jwt:Key";
    }
}