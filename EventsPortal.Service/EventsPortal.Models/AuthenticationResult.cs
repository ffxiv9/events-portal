﻿using System.Net;

namespace EventsPortal.Models
{
    public sealed class AuthenticationResult
    {
        public HttpStatusCode StatusCode { get; }
        public string ErrorMessage { get; }
        public string Token { get; }

        public AuthenticationResult(HttpStatusCode statusCode, string token = null, string errorMessage = null)
        {
            StatusCode = statusCode;
            ErrorMessage = errorMessage;
            Token = token;
        }
    }
}