﻿using System.Text.Json.Serialization;

namespace EventsPortal.Models
{
    public class UserInfo
    {
        [JsonPropertyName("staff_id")]
        public int StaffId { get; set; }

        [JsonPropertyName("first_name")]
        public string FirstName { get; set; }

        [JsonPropertyName("last_name")]
        public string LastName { get; set; }

        [JsonPropertyName("email")]
        public string Email { get; set; }

        [JsonPropertyName("location")]
        public string Location { get; set; }

        [JsonPropertyName("position")]
        public string Position { get; set; }
    }
}