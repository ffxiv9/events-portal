﻿namespace EventsPortal.Models
{
    public class EventUser
    {
        public int UserId { get; set; }
        public virtual User User { get; set; }
        public int EventId { get; set; }
        public virtual Event Event { get; set; }
    }
}