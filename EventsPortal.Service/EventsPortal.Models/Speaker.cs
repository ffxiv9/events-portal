﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace EventsPortal.Models
{
    public class Speaker
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string Position { get; set; }
        public string Description { get; set; }
        public int EventId { get; set; }

        [JsonIgnore]
        public Event Event { get; set; }
    }
}
