﻿using System.Collections.Generic;

namespace EventsPortal.Models.ViewModels
{
    public class LocationViewModel
    {
        public List<string> Locations { get; set; }
    }
}
