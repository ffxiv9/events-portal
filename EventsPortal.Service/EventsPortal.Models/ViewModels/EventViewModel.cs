﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EventsPortal.Models.DataAnnotations;

namespace EventsPortal.Models.ViewModels
{
    public class EventViewModel
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} is required")]
        [DateEqualOrGreater]
        public DateTime StartDate { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} is required")]
        public string Title { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} is required")]
        public string Description { get; set; }

        [Required(ErrorMessage = "{0} is required")]
        public EventVenueType VenueType { get; set; }

        public string Image { get; set; }

        public string LinkBroadcast { get; set; }

        public string SpeakerName { get; set; }

        [Required(ErrorMessage = "{0} is required")]
        public int Duration { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} is required")]
        public string Location { get; set; }

        public ICollection<Speaker> Speakers { get; set; }

        public ContactPersonViewModel ContactPerson { get; set; }
        public bool CanEdit { get; set; }
        public bool IsUserAttending { get; set; }
    }
}