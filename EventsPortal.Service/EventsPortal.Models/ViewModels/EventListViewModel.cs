﻿using System;

namespace EventsPortal.Models.ViewModels
{
    public class EventListViewModel
    {
        public int Id { get; set; }
        public DateTime StartDate { get; set; }
        public string Title { get; set; }
        public EventVenueType VenueType { get; set; }
        public string Image { get; set; }
        public string Location { get; set; }
        public bool CanEdit { get; set; }
        public bool IsUserAttending { get; set; }
    }
}