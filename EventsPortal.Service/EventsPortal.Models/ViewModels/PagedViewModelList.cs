﻿using System.Collections.Generic;

namespace EventsPortal.Models.ViewModels
{
    public class PagedViewModelList<T>
    {
        public List<T> List { get; set; }
        public int FullCount { get; set; }
    }
}
