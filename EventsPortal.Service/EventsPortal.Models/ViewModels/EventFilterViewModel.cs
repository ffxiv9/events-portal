﻿using System;

namespace EventsPortal.Models.ViewModels
{
    public class EventFilterViewModel
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public DateTime? MinDate { get; set; }
        public DateTime? MaxDate { get; set; }
        public string Title { get; set; }
        public EventVenueType? VenueType { get; set; }
        public string Location { get; set; }
    }
}
