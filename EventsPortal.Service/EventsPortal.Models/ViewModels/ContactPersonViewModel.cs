﻿namespace EventsPortal.Models.ViewModels
{
    public class ContactPersonViewModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Position { get; set; }
        public string Email { get; set; }
    }
}