﻿using System.ComponentModel.DataAnnotations;

namespace EventsPortal.Models.ViewModels
{
    public class UserViewModel
    {
        [Required(AllowEmptyStrings = false)]
        [StringLength(35, MinimumLength = 9)]
        public string Username { get; set; }

        [Required(AllowEmptyStrings = false)]
        [StringLength(30, MinimumLength = 6)]
        public string Password { get; set; }
    }
}