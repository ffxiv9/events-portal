﻿using EventsPortal.Models.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EventsPortal.Services
{
    public interface ILocationService
    {
        Task<List<string>> Get();
    }
}
