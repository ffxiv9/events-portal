﻿using System.Threading.Tasks;
using EventsPortal.Data.Interfaces;
using EventsPortal.Models;
using EventsPortal.Services.Exceptions;
using EventsPortal.Services.Interfaces;

namespace EventsPortal.Services
{
    public class EventsPermissionService : IPermissionService<Event>
    {
        private readonly IRepository<User> _userRepository;
        private readonly AuthenticatedUser _user;
        private User _localUser;

        public EventsPermissionService(IRepository<User> userRepository, AuthenticatedUser user)
        {
            _userRepository = userRepository;
            _user = user;
        }

        public async Task<bool> HasUserPermissionToEdit(Event model)
        {
            if (!_user.IsAuthenticated)
                return false;
            if (_user.UserId == model.CreatorId)
                return true;
            if (!_user.IsAdmin)
                return false;

            _localUser ??= await _userRepository.GetById(_user.UserId.Value);
            if (_localUser == null)
                return false;

            return model.Location == _localUser.Location;
        }

        public async Task CheckPermissionToEdit(Event model)
        {
            var canUserEdit = await HasUserPermissionToEdit(model);
            if (!canUserEdit)
                throw new AccessDeniedException("User does not have access privileges");
        }
    }
}