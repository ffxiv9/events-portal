﻿namespace EventsPortal.Services.Extensions
{
    public static class StringExtensions
    {
        public static bool IsNotNull(this string obj)
        {
            return !string.IsNullOrWhiteSpace(obj);
        }
        
        public static bool IsNull(this string obj)
        {
            return string.IsNullOrWhiteSpace(obj);
        }
    }
}