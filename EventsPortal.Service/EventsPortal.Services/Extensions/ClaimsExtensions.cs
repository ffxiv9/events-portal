﻿using System.Security.Claims;

namespace EventsPortal.Services.Extensions
{
    public static class ClaimsExtensions
    {
        public static int GetUserId(this ClaimsPrincipal principal)
        {
            int.TryParse(principal.Identity.Name, out var result);
            return result;
        }
    }
}