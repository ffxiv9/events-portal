﻿using System;

namespace EventsPortal.Services
{
    public static class UrlUtility
    {
        public static bool IsUrlValid(string absoluteUrl)
        {
            return Uri.TryCreate(absoluteUrl, UriKind.Absolute, out var uriResult) &&
                   (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps) &&
                   absoluteUrl.EndsWith("/");
        }
    }
}