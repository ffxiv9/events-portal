﻿using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Threading.Tasks;
using AutoMapper;
using EventsPortal.Data.Interfaces;
using EventsPortal.Models;
using EventsPortal.Services.Authentication;
using EventsPortal.Services.Extensions;
using EventsPortal.Services.Interfaces;
using Microsoft.Extensions.Logging;

namespace EventsPortal.Services
{
    public class UserService : IUserService
    {
        private readonly HttpClient _httpClient;
        private readonly ILogger<UserService> _logger;
        private readonly IMapper _mapper;
        private readonly string _userDetailsUrl;
        private readonly IRepository<User> _userRepository;

        public UserService(HttpClient httpClient, AuthServiceConfiguration configuration, ILogger<UserService> logger,
            IMapper mapper, IRepository<User> userRepository)
        {
            _httpClient = httpClient;
            _logger = logger;
            _mapper = mapper;
            _userRepository = userRepository;
            _httpClient.DefaultRequestHeaders.Accept.Clear();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            if (!UrlUtility.IsUrlValid(configuration.UserDetailsUrl))
                throw new ArgumentException("Invalid user information Url");

            _userDetailsUrl = configuration.UserDetailsUrl;
        }

        public async Task<UserInfo> GetUserInfo(string username)
        {
            if (username.IsNull()) throw new ArgumentNullException(nameof(username));
            string data = null;
            try
            {
                data = await _httpClient.GetStringAsync(string.Format(_userDetailsUrl, username));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
            }

            return data.IsNull() ? null : JsonSerializer.Deserialize<UserInfo>(data);
        }

        public async Task<User> CreateOrUpdateLocalUser(UserInfo user)
        {
            if (user == null || user.StaffId <= 0)
                throw new ArgumentNullException(nameof(user));

            var localUser = await _userRepository.GetById(user.StaffId);
            if (localUser == null)
            {
                localUser = _mapper.Map<User>(user);
                return await _userRepository.Create(localUser) ? localUser : null;
            }

            localUser = _mapper.Map(user, localUser);
            return await _userRepository.Update(localUser) ? localUser : null;
        }

        public async Task<User> GetLocalUser(int userId)
        {
            return userId <= 0
                ? throw new ArgumentNullException(nameof(userId))
                : await _userRepository.GetById(userId);
        }
    }
}