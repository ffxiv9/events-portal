﻿using System.Threading.Tasks;
using EventsPortal.Models;
using EventsPortal.Models.ViewModels;

namespace EventsPortal.Services.Interfaces
{
    public interface IEventsService
    {
        Task<PagedViewModelList<EventListViewModel>> Get(EventFilterViewModel model);
        Task<EventViewModel> Get(int eventId);
        Task Create(EventViewModel model);
        Task Update(int eventId, EventViewModel model);
        Task<Event> Attend(int eventId);
        Task UnAttend(int eventId);
    }
}