﻿using System.Threading.Tasks;

namespace EventsPortal.Services.Interfaces
{
    public interface IPermissionService<in TModel>
        where TModel : class, new()
    {
        Task<bool> HasUserPermissionToEdit(TModel model);
        Task CheckPermissionToEdit(TModel model);
    }
}