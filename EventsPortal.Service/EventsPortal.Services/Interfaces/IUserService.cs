﻿using System.Threading.Tasks;
using EventsPortal.Models;

namespace EventsPortal.Services.Interfaces
{
    public interface IUserService
    {
        Task<UserInfo> GetUserInfo(string username);
        Task<User> CreateOrUpdateLocalUser(UserInfo user);
        Task<User> GetLocalUser(int userId);
    }
}