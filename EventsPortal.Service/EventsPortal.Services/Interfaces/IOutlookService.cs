﻿using EventsPortal.Models;

namespace EventsPortal.Services.Interfaces
{
    public interface IOutlookService
    {
        byte[] GetCalendar(string origin, Event currentEvent, int offset);
    }
}