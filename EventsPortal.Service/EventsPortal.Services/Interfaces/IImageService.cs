﻿using System.Threading.Tasks;

namespace EventsPortal.Services.Interfaces
{
    public interface IImageService
    {
        Task<string> SaveImage(string fileName, string image);
        string GetUniqueName();
        bool IsForSave(string image);
    }
}