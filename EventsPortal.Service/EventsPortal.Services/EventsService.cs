﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using EventsPortal.Data.Interfaces;
using EventsPortal.Data.Specifications;
using EventsPortal.Models;
using EventsPortal.Models.ViewModels;
using EventsPortal.Services.Exceptions;
using EventsPortal.Services.Interfaces;

namespace EventsPortal.Services
{
    public class EventsService : IEventsService
    {
        private readonly IEventUserRepository _eventUserRepository;
        private readonly IImageService _imageService;
        private readonly IPermissionService<Event> _permissionService;
        private readonly IMapper _mapper;
        private readonly IEventRepository _repository;
        private readonly AuthenticatedUser _user;

        public EventsService(
            IEventRepository repository,
            IMapper mapper,
            IImageService imageService,
            IPermissionService<Event> permissionService,
            IEventUserRepository eventUserRepository,
            AuthenticatedUser user)
        {
            _repository = repository;
            _mapper = mapper;
            _imageService = imageService;
            _permissionService = permissionService;
            _eventUserRepository = eventUserRepository;
            _user = user;
        }

        public async Task<PagedViewModelList<EventListViewModel>> Get(EventFilterViewModel model)
        {
            Expression<Func<Event, bool>> expression = e =>
                e.Location.Contains(model.Location ?? string.Empty)
                && e.StartDate >= (model.MinDate ?? e.StartDate)
                && e.StartDate <= (model.MaxDate ?? e.StartDate)
                && e.Title.Contains(model.Title ?? string.Empty)
                && e.VenueType != (EventVenueType.OnlineAndOffline ^ model.VenueType ?? default);

            var events = await _repository.GetEvents(expression, model.PageSize, model.PageNumber);
            var count = await _repository.GetCount(expression);

            var viewModels = new List<EventListViewModel>();
            foreach (var curEvent in events)
            {
                var viewModel = _mapper.Map<EventListViewModel>(curEvent);
                viewModel.CanEdit = await _permissionService.HasUserPermissionToEdit(curEvent);
                viewModel.IsUserAttending = await _eventUserRepository.IsUserAttending(_user.UserId.Value, curEvent.Id);
                viewModels.Add(viewModel);
            }

            var eventsPage = new PagedViewModelList<EventListViewModel>
            {
                List = viewModels,
                FullCount = count
            };

            return eventsPage;
        }

        public async Task<EventViewModel> Get(int eventId)
        {
            var currentEvent = await _repository.Get(new EventWithSpeakersSpecification(eventId));
            if (currentEvent == null)
                throw new ResourceNotFoundException("Event not found");

            var eventViewModel = _mapper.Map<EventViewModel>(currentEvent);
            eventViewModel.CanEdit = await _permissionService.HasUserPermissionToEdit(currentEvent);
            eventViewModel.IsUserAttending = await _eventUserRepository.IsUserAttending(_user.UserId.Value, eventId);
            eventViewModel.ContactPerson = _mapper.Map<ContactPersonViewModel>(currentEvent.Creator);
            return eventViewModel;
        }

        public async Task Create(EventViewModel model)
        {
            var eventModel = _mapper.Map<Event>(model);
            eventModel.CreatorId = _user.UserId.Value;
            eventModel.CreatedDate = DateTime.UtcNow;
            eventModel.Image = await _imageService.SaveImage(_imageService.GetUniqueName(), model.Image);
            await HandleSpeakerImages(model.Speakers);

            if (!await _repository.Create(eventModel))
                throw new OperationException("An error occurred while creating the event");
        }

        public async Task Update(int eventId, EventViewModel model)
        {
            var stored = await _repository.Get(new EventWithSpeakersSpecification(eventId));
            if (stored == null)
                throw new ResourceNotFoundException("Event with this id does not exist");

            await _permissionService.CheckPermissionToEdit(stored);

            if (_imageService.IsForSave(model.Image))
                model.Image = await _imageService.SaveImage(_imageService.GetUniqueName(), model.Image);

            var eventModel = _mapper.Map(model, stored);
            await HandleSpeakerImages(eventModel.Speakers);

            if (!await _repository.Update(eventModel))
                throw new OperationException("An error occurred while updating the event");
        }

        private Task HandleSpeakerImages(IEnumerable<Speaker> speakers)
        {
            var tasks = new List<Task>();
            foreach (var speaker in speakers)
            {
                if (_imageService.IsForSave(speaker.Image))
                {
                    var name = _imageService.GetUniqueName();
                    tasks.Add(_imageService.SaveImage(name, speaker.Image));
                    speaker.Image = name;
                }
            }

            return Task.WhenAll(tasks.ToArray());
        }

        public async Task<Event> Attend(int eventId)
        {
            var eventUser = new EventUser {EventId = eventId, UserId = _user.UserId.Value};
            await _eventUserRepository.Create(eventUser);
            var currentEvent = await _repository.GetById(eventId);
            return currentEvent;
        }

        public async Task UnAttend(int eventId)
        {
            var eventUser = new EventUser {EventId = eventId, UserId = _user.UserId.Value};
            await _eventUserRepository.Delete(eventUser);
        }
    }
}