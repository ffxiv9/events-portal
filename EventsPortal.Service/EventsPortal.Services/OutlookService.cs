using System;
using System.Text;
using EventsPortal.Models;
using EventsPortal.Models.Constants;
using EventsPortal.Services.Interfaces;
using Ical.Net;
using Ical.Net.CalendarComponents;
using Ical.Net.DataTypes;
using Ical.Net.Serialization;

namespace EventsPortal.Services
{
    public class OutlookService : IOutlookService
    {
        public byte[] GetCalendar(string url, Event currentEvent, int offset)
        {
            var firstAlarm = new Alarm {Trigger = new Trigger(TimeSpan.FromMinutes(-15)), Action = AlarmAction.Display};
            var secondAlarm = new Alarm {Trigger = new Trigger(TimeSpan.FromDays(-1)), Action = AlarmAction.Display};

            var calendarEvent = new CalendarEvent
            {
                Summary = currentEvent.Title,
                Description = url,
                Location = currentEvent.VenueType.ToString(),
                IsAllDay = false,
                DtStart = new CalDateTime(currentEvent.StartDate.AddMinutes(offset)),
                Duration = new TimeSpan(0, currentEvent.Duration, 0)
            };

            if (currentEvent.VenueType != EventVenueType.Offline)
                calendarEvent.Description += $"\n\n{OutlookConstants.OnlineEventAlarmInfoAddition}";
            calendarEvent.Alarms.Add(firstAlarm);
            calendarEvent.Alarms.Add(secondAlarm);

            var calendar = new Calendar();
            calendar.Events.Add(calendarEvent);

            var serializer = new CalendarSerializer();
            var serializedCalendar = serializer.SerializeToString(calendar);
            var calendarArray = Encoding.UTF8.GetBytes(serializedCalendar.ToCharArray());
            return calendarArray;
        }
    }
}