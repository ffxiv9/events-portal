﻿using System;

namespace EventsPortal.Services.Exceptions
{
    public class OperationException : Exception
    {
        public OperationException(string message) : base(message)
        {
        }
    }
}