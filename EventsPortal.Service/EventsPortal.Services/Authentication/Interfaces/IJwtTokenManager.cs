﻿namespace EventsPortal.Services.Authentication.Interfaces
{
    public interface IJwtTokenManager
    {
        string GenerateToken(int userId, string role);
    }
}