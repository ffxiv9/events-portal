﻿using System.Threading.Tasks;
using EventsPortal.Models;

namespace EventsPortal.Services.Authentication.Interfaces
{
    public interface IAuthService
    {
        Task<AuthenticationResult> GetToken(string username, string password);
    }
}