﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Threading.Tasks;
using EventsPortal.Models;
using EventsPortal.Services.Authentication.Interfaces;
using Microsoft.Extensions.Logging;
using static System.Text.Encoding;

namespace EventsPortal.Services.Authentication
{
    public class AuthService : IAuthService
    {
        private readonly HttpClient _httpClient;
        private readonly ILogger<AuthService> _logger;
        private readonly string _tokenUrl;

        public AuthService(HttpClient httpClient, AuthServiceConfiguration configuration, ILogger<AuthService> logger)
        {
            _httpClient = httpClient;
            _logger = logger;
            _httpClient.DefaultRequestHeaders.Accept.Clear();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            if (!UrlUtility.IsUrlValid(configuration.TokenUrl)) throw new ArgumentException("Invalid token Url");

            _tokenUrl = configuration.TokenUrl;
        }

        public async Task<AuthenticationResult> GetToken(string username, string password)
        {
            var content = new StringContent(JsonSerializer.Serialize(new {username, password}), UTF8,
                "application/json");
            var request = new HttpRequestMessage(HttpMethod.Post, _tokenUrl) {Content = content};
            try
            {
                var response = await _httpClient.SendAsync(request);
                var token = response.IsSuccessStatusCode ? await response.Content.ReadAsStringAsync() : null;
                return new AuthenticationResult(response.StatusCode, token);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return new AuthenticationResult(HttpStatusCode.BadGateway, null, "Integration Service is unavailable.");
            }
        }
    }
}