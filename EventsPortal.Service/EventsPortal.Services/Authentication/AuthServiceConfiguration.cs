﻿namespace EventsPortal.Services.Authentication
{
    public class AuthServiceConfiguration
    {
        public string TokenUrl { get; set; }
        public string UserDetailsUrl { get; set; }
    }
}