﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using EventsPortal.Models.Constants;
using EventsPortal.Services.Authentication.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace EventsPortal.Services.Authentication
{
    public class JwtTokenManager : IJwtTokenManager
    {
        private readonly IConfiguration _configuration;

        public JwtTokenManager(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public string GenerateToken(int userId, string role)
        {
            if (userId <= 0) throw new ArgumentOutOfRangeException(nameof(userId));

            var securityKey =
                new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration[JwtSettingConstants.SecretKey]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, userId.ToString()),
                new Claim(RoleDefaults.DefaultClaim, role ?? RoleDefaults.DefaultRole)
            };

            var newToken = new JwtSecurityToken(_configuration[JwtSettingConstants.Issuer],
                _configuration[JwtSettingConstants.Audience], claims, expires: DateTime.UtcNow.AddDays(1),
                signingCredentials: credentials);
            return new JwtSecurityTokenHandler().WriteToken(newToken);
        }
    }
}