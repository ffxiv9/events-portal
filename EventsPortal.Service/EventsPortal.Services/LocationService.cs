﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using EventsPortal.Models.ViewModels;
using EventsPortal.Services.Extensions;
using Microsoft.Extensions.Logging;

namespace EventsPortal.Services
{
    public class LocationService : ILocationService
    {
        private readonly HttpClient _httpClient;
        private readonly string _locationUrl;

        public LocationService(HttpClient httpClient, LocationServiceConfiguration configuration)
        {
            _httpClient = httpClient;

            if (!UrlUtility.IsUrlValid(configuration.LocationUrl))
                throw new ArgumentException("Invalid locations Url");

            _locationUrl = configuration.LocationUrl;
        }

        public async Task<List<string>> Get()
        {
            var responseMessage = await _httpClient.GetAsync(string.Format(_locationUrl));
            responseMessage.EnsureSuccessStatusCode();

            var response = await responseMessage.Content.ReadAsStringAsync();
            return response.IsNull() ? null : JsonSerializer.Deserialize<List<string>>(response);
        }
    }
}