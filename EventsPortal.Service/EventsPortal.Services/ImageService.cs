﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Threading.Tasks;
using EventsPortal.Services.Interfaces;
using Microsoft.Extensions.Logging;
using static System.Text.Encoding;

namespace EventsPortal.Services
{
    public class ImageService : IImageService
    {
        private readonly HttpClient _httpClient;
        private readonly ILogger<ImageService> _logger;
        private readonly string _saveImageUrl;

        public ImageService(HttpClient httpClient, ImageServiceConfiguration configuration,
            ILogger<ImageService> logger)
        {
            _httpClient = httpClient;
            _logger = logger;
            _httpClient.DefaultRequestHeaders.Accept.Clear();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            _saveImageUrl = configuration.SaveImageUrl;
        }

        public async Task<string> SaveImage(string fileName, string image)
        {
            if (image == null)
                return null;
            var content = new StringContent(JsonSerializer.Serialize(new {name = fileName, file = image}), UTF8,
                "application/json");
            var request = new HttpRequestMessage(HttpMethod.Post, _saveImageUrl) {Content = content};

            try
            {
                var response = await _httpClient.SendAsync(request);
                if (!response.IsSuccessStatusCode)
                    throw new ArgumentException("Invalid url or integration service is unavailable");
                return fileName;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return null;
            }
        }

        public bool IsForSave(string image)
        {
            return image != null && !Guid.TryParse(image, out _);
        }

        public string GetUniqueName()
        {
            return Guid.NewGuid().ToString();
        }
    }
}