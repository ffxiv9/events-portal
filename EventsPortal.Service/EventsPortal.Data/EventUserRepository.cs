﻿using System.Threading.Tasks;
using EventsPortal.Data.Interfaces;
using EventsPortal.Models;
using Microsoft.EntityFrameworkCore;

namespace EventsPortal.Data
{
    public class EventUserRepository : Repository<EventUser>, IEventUserRepository
    {
        public EventUserRepository(DbContext context) : base(context)
        {
        }

        public async Task<bool> IsUserAttending(int userId, int eventId)
        {
            return await _dbSet.AnyAsync(x => x.UserId == userId && x.EventId == eventId);
        }
    }
}