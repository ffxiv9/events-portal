﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using EventsPortal.Data.Interfaces;
using EventsPortal.Data.Specifications;
using Microsoft.EntityFrameworkCore;

namespace EventsPortal.Data
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly DbContext _context;
        protected readonly DbSet<TEntity> _dbSet;

        public Repository(DbContext context)
        {
            _context = context;
            _dbSet = context.Set<TEntity>();
        }

        public async Task<List<TEntity>> Get()
        {
            return await _dbSet.AsNoTracking().ToListAsync();
        }

        public async Task<TEntity> GetById(object id)
        {
            return await _dbSet.FindAsync(id);
        }

        public async Task<List<TEntity>> Get(Expression<Func<TEntity, bool>> expression)
        {
            return await _dbSet.Where(expression).AsNoTracking().ToListAsync();
        }

        public async Task<TEntity> Get(ISpecification<TEntity> spec)
        {
            var queryableResultWithIncludes = spec.Includes
                .Aggregate(_context.Set<TEntity>().AsQueryable(),
                    (current, include) => current.Include(include));

            var secondaryResult = spec.IncludeStrings
                .Aggregate(queryableResultWithIncludes,
                    (current, include) => current.Include(include));

            return await secondaryResult
                .Where(spec.Criteria)
                .SingleOrDefaultAsync();
        }

        public async Task<bool> Create(TEntity entity)
        {
            await _dbSet.AddAsync(entity);
            return await Save();
        }

        public virtual async Task<bool> Update(TEntity entity)
        {
            await Task.Run(() => { _dbSet.Update(entity); });
            return await Save();
        }

        public async Task<bool> Delete(TEntity entity)
        {
            await Task.Run(() => { _dbSet.Remove(entity); });
            return await Save();
        }

        protected async Task<bool> Save()
        {
            try
            {
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}