﻿using EventsPortal.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace EventsPortal.Data
{
    public class EventsPortalDbContext : DbContext
    {
        private static readonly ILoggerFactory LoggerFactory
            = Microsoft.Extensions.Logging.LoggerFactory.Create(builder =>
            {
                builder
                    .AddFilter((category, level) =>
                        category == DbLoggerCategory.Database.Command.Name && level == LogLevel.Information).AddDebug();
            });

        public EventsPortalDbContext(DbContextOptions<EventsPortalDbContext> options)
            : base(options)
        {
        }

        public DbSet<Event> Events { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<EventUser> EventUser { get; set; }
        public DbSet<Speaker> Speakers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLoggerFactory(LoggerFactory);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasKey(u => u.Id);

            modelBuilder.Entity<Event>()
                .Ignore(e => e.SpeakerName);
            modelBuilder.Entity<Event>()
                .HasKey(e => e.Id);
            modelBuilder.Entity<Event>().Property(e => e.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<Event>().Property(e => e.CreatorId).IsRequired();

            modelBuilder.Entity<Speaker>()
                .HasKey(s => s.Id);
            modelBuilder.Entity<Speaker>().Property(s => s.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<Speaker>()
                .HasOne(s => s.Event)
                .WithMany(e => e.Speakers)
                .HasForeignKey(s => s.EventId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<EventUser>()
                .HasKey(eu => new {eu.UserId, eu.EventId});
            modelBuilder.Entity<EventUser>()
                .HasOne(eu => eu.Event)
                .WithMany(e => e.EventUsers)
                .HasForeignKey(eu => eu.EventId)
                .OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<EventUser>()
                .HasOne(eu => eu.User)
                .WithMany(e => e.EventUsers)
                .HasForeignKey(eu => eu.UserId)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}