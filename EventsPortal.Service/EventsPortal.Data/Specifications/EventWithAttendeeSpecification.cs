﻿using EventsPortal.Models;

namespace EventsPortal.Data.Specifications
{
    public class EventWithAttendeeSpecification : BaseSpecification<Event>
    {
        public EventWithAttendeeSpecification(int eventId)
            : base(e => e.Id == eventId)
        {
            AddInclude(e => e.EventUsers);
        }
    }
}
