﻿using EventsPortal.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EventsPortal.Data.Specifications
{
    public class EventWithSpeakersSpecification : BaseSpecification<Event>
    {
        public EventWithSpeakersSpecification(int eventId)
           : base(e => e.Id == eventId)
        {
            AddInclude(e => e.Speakers);
            AddInclude(e => e.Creator);
        }
    }
}
