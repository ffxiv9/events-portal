﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EventsPortal.Data.Migrations
{
    public partial class ChangeUserPK : Migration
    {
        private readonly Dictionary<string, int> _userIds = new Dictionary<string, int>
        {
            {"maxandix", 1},
            {"portal_maxandix", 2},
            {"dev_mikhail", 3},
            {"new_user1", 4},
            {"nastyakuzya", 5},
            {"design_team", 6},
            {"quality_assurance", 7},
            {"usernameusername", 8},
            {"test_user", 9},
            {"dima_user", 10},
            {"max.biryukov", 11},
            {"dima_user2", 12}
        };

        protected override void Up(MigrationBuilder migrationBuilder)
        {
            #region DropConstaints

            migrationBuilder.DropForeignKey(
                "FK_Events_Users_CreatorLogin",
                "Events");

            migrationBuilder.DropForeignKey(
                "FK_EventUser_Users_UserId",
                "EventUser");

            migrationBuilder.DropPrimaryKey(
                "PK_Users",
                "Users");

            migrationBuilder.DropPrimaryKey(
                "PK_EventUser",
                "EventUser");

            migrationBuilder.DropIndex(
                "IX_Events_CreatorLogin",
                "Events");

            #endregion

            #region AddNewColumns

            migrationBuilder.AddColumn<int>(
                "Id",
                "Users",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                "UserId_new",
                "EventUser",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                "CreatorId",
                "Events",
                nullable: false,
                defaultValue: 0);

            #endregion

            #region Relinking

            foreach (var username in _userIds.Keys)
            {
                migrationBuilder.Sql($"UPDATE Users SET Users.Id = {_userIds[username]} WHERE Users.Login = '{username}';");
                migrationBuilder.Sql("UPDATE Events SET Events.CreatorId = Users.Id FROM Events INNER JOIN Users ON Events.CreatorLogin = Users.Login;");
                migrationBuilder.Sql("UPDATE EventUser SET EventUser.UserId_new = Users.Id FROM EventUser INNER JOIN Users ON EventUser.UserId = Users.Login;");
            }

            migrationBuilder.Sql("DELETE FROM Users WHERE Id = 0;");

            #endregion

            #region RemoveOldColumns

            migrationBuilder.DropColumn(
                "UserId",
                "EventUser");

            migrationBuilder.RenameColumn(
                "UserId_new",
                "EventUser",
                "UserId");

            migrationBuilder.DropColumn(
                "Login",
                "Users");

            migrationBuilder.DropColumn(
                "CreatorLogin",
                "Events");

            #endregion

            #region AddConstraints

            migrationBuilder.AddPrimaryKey(
                "PK_Users",
                "Users",
                "Id");

            migrationBuilder.AddPrimaryKey(
                "PK_EventUser",
                "EventUser",
                new[] {"UserId", "EventId"});

            migrationBuilder.CreateIndex(
                "IX_Events_CreatorId",
                "Events",
                "CreatorId");

            migrationBuilder.AddForeignKey(
                "FK_Events_Users_CreatorId",
                "Events",
                "CreatorId",
                "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                "FK_EventUser_Users_UserId",
                "EventUser",
                "UserId",
                "Users",
                principalColumn: "Id");

            #endregion
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                "FK_Events_Users_CreatorId",
                "Events");

            migrationBuilder.DropForeignKey(
                "FK_EventUser_Users_UserId",
                "EventUser");

            migrationBuilder.DropPrimaryKey(
                "PK_EventUser",
                "EventUser");

            migrationBuilder.DropPrimaryKey(
                "PK_Users",
                "Users");

            migrationBuilder.DropIndex(
                "IX_Events_CreatorId",
                "Events");

            migrationBuilder.DropColumn(
                "Id",
                "Users");

            migrationBuilder.DropColumn(
                "CreatorId",
                "Events");

            migrationBuilder.AddColumn<string>(
                "Login",
                "Users",
                "nvarchar(450)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<string>(
                "UserId",
                "EventUser",
                "nvarchar(450)",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<string>(
                "CreatorLogin",
                "Events",
                "nvarchar(450)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddPrimaryKey(
                "PK_Users",
                "Users",
                "Login");

            migrationBuilder.AddPrimaryKey(
                "PK_EventUser",
                "EventUser",
                new[] {"UserId", "EventId"});

            migrationBuilder.CreateIndex(
                "IX_Events_CreatorLogin",
                "Events",
                "CreatorLogin");

            migrationBuilder.AddForeignKey(
                "FK_Events_Users_CreatorLogin",
                "Events",
                "CreatorLogin",
                "Users",
                principalColumn: "Login",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                "FK_EventUser_Users_UserId",
                "EventUser",
                "UserId",
                "Users",
                principalColumn: "Login");
        }
    }
}