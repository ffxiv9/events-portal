﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EventsPortal.Data.Migrations
{
    public partial class AddEventCreator : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            const string testUser = "quality_assurance";
            migrationBuilder.Sql(
                $"INSERT INTO Users (login) SELECT '{testUser}' WHERE NOT exists (SELECT * FROM Users WHERE login='{testUser}')");

            migrationBuilder.AddColumn<string>(
                name: "CreatorLogin",
                table: "Events",
                nullable: false,
                defaultValue: testUser);

            migrationBuilder.CreateIndex(
                name: "IX_Events_CreatorLogin",
                table: "Events",
                column: "CreatorLogin");

            migrationBuilder.AddForeignKey(
                name: "FK_Events_Users_CreatorLogin",
                table: "Events",
                column: "CreatorLogin",
                principalTable: "Users",
                principalColumn: "Login",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Events_Users_CreatorLogin",
                table: "Events");

            migrationBuilder.DropIndex(
                name: "IX_Events_CreatorLogin",
                table: "Events");

            migrationBuilder.DropColumn(
                name: "CreatorLogin",
                table: "Events");
        }
    }
}
