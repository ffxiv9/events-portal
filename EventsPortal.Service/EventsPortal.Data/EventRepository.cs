﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using EventsPortal.Data.Interfaces;
using EventsPortal.Models;
using Microsoft.EntityFrameworkCore;

namespace EventsPortal.Data
{
    public class EventRepository : Repository<Event>, IEventRepository
    {
        public EventRepository(DbContext context) : base(context)
        {
        }

        public async Task<List<Event>> GetEvents(Expression<Func<Event, bool>> expression, int pageSize, int pageNumber)
        {
            var events = await _dbSet
                .Where(expression)
                .OrderBy(e => e.StartDate)
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();

            return events;
        }

        public async Task<int> GetCount(Expression<Func<Event, bool>> expression)
        {
            var count = await _dbSet.Where(expression).CountAsync();

            return count;
        }
    }
}
