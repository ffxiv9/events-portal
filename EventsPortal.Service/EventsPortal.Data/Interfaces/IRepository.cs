﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using EventsPortal.Data.Specifications;

namespace EventsPortal.Data.Interfaces
{
    public interface IRepository<TEntity> where TEntity : class
    {
        Task<TEntity> GetById(object id);
        Task<List<TEntity>> Get();
        Task<List<TEntity>> Get(Expression<Func<TEntity, bool>> expression);
        Task<TEntity> Get(ISpecification<TEntity> specification);
        Task<bool> Create(TEntity entity);
        Task<bool> Update(TEntity entity);
        Task<bool> Delete(TEntity entity);
    }
}