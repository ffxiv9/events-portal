﻿using System.Threading.Tasks;
using EventsPortal.Models;

namespace EventsPortal.Data.Interfaces
{
    public interface IEventUserRepository : IRepository<EventUser>
    {
        Task<bool> IsUserAttending(int userId, int eventId);
    }
}