﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using EventsPortal.Models;

namespace EventsPortal.Data.Interfaces
{
    public interface IEventRepository : IRepository<Event>
    {
        Task<List<Event>> GetEvents(Expression<Func<Event, bool>> expression, int pageSize, int pageNumber);
        Task<int> GetCount(Expression<Func<Event, bool>> expression);
    }
}