using AutoMapper;
using EventsPortal.Models.ViewModels;
using EventsPortal.WebApi.Extensions;
using EventsPortal.WebApi.Filters;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ModelBinding.Binders;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Globalization;
using System.Threading.Tasks;

namespace EventsPortal.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<ComplexTypeModelBinderProvider>();
            services.AddControllers(options =>
            {
                options.Filters.Add(new ErrorHandlerAttribute());
                options.ModelBinderProviders.Insert(
                   0, new EventFilterViewModelBinderProvider());
            })
                .AddJsonOptions(options =>
                {
                    options.JsonSerializerOptions.IgnoreNullValues = true;
                });
            services.AddHttpClient();
            services.AddAccounting(Configuration);

            services.AddAutoMapper();

            services.AddDataAccess();
            services.AddEvents();
            services.AddImage(Configuration);
            services.AddLocations(Configuration);
            services.AddDatabase(Configuration);
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, DbContext context)
        {
            context.Database.Migrate();

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors(builder =>
            {
                builder
                    .WithOrigins(Configuration.GetSection("AllowedOrigns").Value.Split(","))
                    .AllowCredentials()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
            });

            app.UseAuthentication();
            app.UseAuthenticationForServices();
            app.UseAuthorization();

            app.UseEndpoints(endpoints => endpoints.MapDefaultControllerRoute());
        }
    }

    public class EventFilterViewModelBinderProvider : IModelBinderProvider
    {
        public IModelBinder GetBinder(ModelBinderProviderContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            if (context.Metadata.ModelType == typeof(EventFilterViewModel))
            {
                var complexTypeModelBinderProvider = context.Services.GetRequiredService<ComplexTypeModelBinderProvider>();
                var complexTypeModelBinder = complexTypeModelBinderProvider.GetBinder(context);
                return new EventFilterViewModelBinder(complexTypeModelBinder);
            }
            return null;
        }
    }

    public class EventFilterViewModelBinder : IModelBinder
    {
        private readonly IModelBinder _inner;

        public EventFilterViewModelBinder(IModelBinder inner)
        {
            _inner = inner;
        }

        public async Task BindModelAsync(ModelBindingContext bindingContext)
        {
            await _inner.BindModelAsync(bindingContext);
            if (!bindingContext.Result.IsModelSet)
            {
                return;
            }

            EventFilterViewModel filter = bindingContext.Result.Model as EventFilterViewModel;

            if (filter == null)
            {
                throw new InvalidOperationException($"Expected {bindingContext.ModelName} to have been bound by ComplexTypeModelBinder");
            }
            var minDate = (string)bindingContext.ValueProvider.GetValue("minDate");
            var maxDate = (string)bindingContext.ValueProvider.GetValue("maxDate");

            if (!string.IsNullOrEmpty(minDate))
                filter.MinDate = DateTime.Parse(minDate, null, DateTimeStyles.AdjustToUniversal);
            if (!string.IsNullOrEmpty(maxDate))
                filter.MaxDate = DateTime.Parse(maxDate, null, DateTimeStyles.AdjustToUniversal);
        }
    }
}