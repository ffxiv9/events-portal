﻿using System.Text;
using System.Threading.Tasks;
using EventsPortal.Services.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;

namespace EventsPortal.WebApi.Filters
{
    internal sealed class ErrorHandlerAttribute : ExceptionFilterAttribute
    {
        public override async Task OnExceptionAsync(ExceptionContext context)
        {
            switch (context.Exception)
            {
                case AccessDeniedException _:
                    context.HttpContext.Response.StatusCode = StatusCodes.Status403Forbidden;
                    context.ExceptionHandled = true;
                    break;
                case ResourceNotFoundException _:
                    context.HttpContext.Response.StatusCode = StatusCodes.Status404NotFound;
                    context.ExceptionHandled = true;
                    break;
                case OperationException _:
                    context.HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    context.ExceptionHandled = true;
                    var bytes = Encoding.UTF8.GetBytes(context.Exception.Message);
                    await context.HttpContext.Response.Body.WriteAsync(bytes, 0, bytes.Length);
                    break;

                default:
                    throw context.Exception;
            }
        }
    }
}