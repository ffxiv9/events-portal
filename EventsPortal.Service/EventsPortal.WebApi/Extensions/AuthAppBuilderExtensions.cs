﻿using System;
using EventsPortal.WebApi.Middlewares;
using Microsoft.AspNetCore.Builder;

namespace EventsPortal.WebApi.Extensions
{
    public static class AuthAppBuilderExtensions
    {
        public static IApplicationBuilder UseAuthenticationForServices(this IApplicationBuilder app)
        {
            if (app == null) throw new ArgumentNullException(nameof(app));

            return app.UseMiddleware<ServiceAuthenticationMiddleware>();
        }
    }
}