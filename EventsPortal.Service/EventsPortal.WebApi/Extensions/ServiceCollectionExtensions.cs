﻿using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using EventsPortal.Data;
using EventsPortal.Data.Interfaces;
using EventsPortal.Models;
using EventsPortal.Models.Constants;
using EventsPortal.Services;
using EventsPortal.Services.Authentication;
using EventsPortal.Services.Authentication.Interfaces;
using EventsPortal.Services.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace EventsPortal.WebApi.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddAccounting(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateAudience = true, ValidateIssuer = true, ValidateLifetime = true,
                    ValidateIssuerSigningKey = true, RequireSignedTokens = true,
                    ValidAudiences = new[] {configuration[JwtSettingConstants.Audience]},
                    ValidIssuer = configuration[JwtSettingConstants.Issuer],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration[JwtSettingConstants.SecretKey])),

                    // If the token does not have a `sub` claim, `User.Identity.Name` will be `null`.
                    // Map it to a different claim by setting the NameClaimType below.
                    NameClaimType = ClaimTypes.NameIdentifier
                };
                // Check token in request cookies instead of header
                options.Events = new JwtBearerEvents
                {
                    OnMessageReceived = context =>
                    {
                        context.Token = context.Request.Cookies["SESSIONID"];
                        return Task.CompletedTask;
                    }
                };
            });

            services.AddScoped<AuthenticatedUser>();
            services.AddSingleton(configuration.GetSection("AuthProvider").Get<AuthServiceConfiguration>());
            services.AddScoped(e => e.GetService<IHttpClientFactory>().CreateClient());
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IPermissionService<Event>, EventsPermissionService>();
            services.AddSingleton<IJwtTokenManager, JwtTokenManager>();
            return services.AddScoped<IAuthService, AuthService>();
        }

        public static IServiceCollection AddEvents(this IServiceCollection services)
        {
            return services.AddScoped<IEventsService, EventsService>();
        }

        public static IServiceCollection AddLocations(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton(configuration.GetSection("LocationProvider").Get<LocationServiceConfiguration>());
            return services.AddScoped<ILocationService, LocationService>();
        }

        public static IServiceCollection AddDataAccess(this IServiceCollection services)
        {
            services.AddScoped<IRepository<Event>, Repository<Event>>();
            services.AddScoped<IRepository<User>, Repository<User>>();
            services.AddScoped<IRepository<Speaker>, Repository<Speaker>>();
            services.AddScoped<IEventRepository, EventRepository>();
            services.AddScoped<IOutlookService, OutlookService>();  
            services.AddScoped<IEventUserRepository, EventUserRepository>();
            return services.AddScoped<DbContext, EventsPortalDbContext>();
        }

        public static IServiceCollection AddImage(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton(configuration.GetSection("ImageProvider").Get<ImageServiceConfiguration>());
            return services.AddScoped<IImageService, ImageService>();
        }

        public static IServiceCollection AddDatabase(this IServiceCollection services, IConfiguration configuration)
        {
            return services.AddDbContext<EventsPortalDbContext>(options =>
                options.UseSqlServer(configuration.GetConnectionString("EventsPortalDatabase")));
        }
    }
}