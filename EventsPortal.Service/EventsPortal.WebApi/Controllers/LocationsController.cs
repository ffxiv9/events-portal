﻿using System.Threading.Tasks;
using EventsPortal.Services;
using EventsPortal.Services.Exceptions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace EventsPortal.WebApi.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    public class LocationsController : ControllerBase
    {
        private readonly ILocationService _locationService;

        public LocationsController(ILocationService locationService)
        {
            _locationService = locationService;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var locations = await _locationService.Get();
            if (locations == null || locations.Count == 0)
                throw new ResourceNotFoundException("There are no locations");

            return Ok(locations);
        }
    }
}