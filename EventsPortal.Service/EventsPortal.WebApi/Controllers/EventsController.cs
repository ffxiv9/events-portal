using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using EventsPortal.Models.ViewModels;
using EventsPortal.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace EventsPortal.WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Authorize]
    public class EventsController : ControllerBase
    {
        private readonly IEventsService _eventsService;
        private readonly IOutlookService _outlookService;

        public EventsController(IEventsService eventsService, IOutlookService outlookService)
        {
            _eventsService = eventsService;
            _outlookService = outlookService;
        }

        [HttpGet]
        public async Task<ActionResult<PagedViewModelList<EventListViewModel>>> Get(
            [FromQuery] EventFilterViewModel eventFilter)
        {
            var eventsVm = await _eventsService.Get(eventFilter);
            return Ok(eventsVm);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get([Range(1, int.MaxValue)][FromRoute] int id)
        {
            var eventVm = await _eventsService.Get(id);
            return Ok(eventVm);
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] EventViewModel eventModel)
        {
            await _eventsService.Create(eventModel);
            return Ok(eventModel);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Edit([FromRoute][Range(1, int.MaxValue)] int id,
            [FromBody] EventViewModel eventModel)
        {
            await _eventsService.Update(id, eventModel);
            return NoContent();
        }

        [HttpPut("attend/{id}/{offset}")]
        public async Task<IActionResult> Attend([Range(1, int.MaxValue)][FromRoute] int id, int offset)
        {
            Request.Headers.TryGetValue("Origin", out var origin);
            var originUrl = $"{origin}/event/{id}";

            var currentEvent = await _eventsService.Attend(id);
            var calendar = _outlookService.GetCalendar(originUrl, currentEvent, offset);

            return File(calendar, "text/ics");
        }
        
        [HttpPut("unattend/{id}")]
        public async Task<IActionResult> UnAttend([Range(1, int.MaxValue)][FromRoute] int id)
        {
            await _eventsService.UnAttend(id);
            return NoContent();
        }
    }
}