using System;
using System.Net;
using System.Threading.Tasks;
using EventsPortal.Models.ViewModels;
using EventsPortal.Services.Authentication.Interfaces;
using EventsPortal.Services.Extensions;
using EventsPortal.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace EventsPortal.WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class UsersController : ControllerBase
    {
        private readonly IAuthService _authService;
        private readonly IJwtTokenManager _tokenManager;
        private readonly IUserService _userService;

        public UsersController(IAuthService authService, IJwtTokenManager tokenManager, IUserService userService)
        {
            _authService = authService;
            _tokenManager = tokenManager;
            _userService = userService;
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Login([FromBody] UserViewModel model)
        {
            var authenticationResult = await _authService.GetToken(model.Username, model.Password);
            if (authenticationResult.StatusCode != HttpStatusCode.OK || authenticationResult.Token.IsNull())
                return StatusCode((int) authenticationResult.StatusCode, authenticationResult.ErrorMessage);

            var userInfo = await _userService.GetUserInfo(model.Username);
            if (userInfo == null)
                throw new InvalidOperationException("Unable to retrieve requested user information");

            var user = await _userService.CreateOrUpdateLocalUser(userInfo);
            if (user == null)
                throw new InvalidOperationException("Error occurred while updating user information");

            var token = _tokenManager.GenerateToken(user.Id, user.Position);
            return Ok(new {token});
        }
    }
}