﻿using AutoMapper;
using EventsPortal.Models;
using EventsPortal.Models.ViewModels;

namespace EventsPortal.WebApi.Mappers
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<Event, EventViewModel>().ReverseMap();
            CreateMap<Event, EventListViewModel>().ReverseMap();
            CreateMap<User, UserInfo>()
                .ForMember(e => e.StaffId, options => options.MapFrom(s => s.Id)).ReverseMap();
            CreateMap<User, ContactPersonViewModel>();
        }
    }
}