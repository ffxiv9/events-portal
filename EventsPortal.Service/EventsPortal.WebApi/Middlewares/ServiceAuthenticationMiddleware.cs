﻿using System.Threading.Tasks;
using EventsPortal.Models;
using EventsPortal.Models.Constants;
using EventsPortal.Services.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace EventsPortal.WebApi.Middlewares
{
    public class ServiceAuthenticationMiddleware
    {
        private readonly RequestDelegate _next;

        public ServiceAuthenticationMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            if (httpContext.User != null && httpContext.User.Identity.IsAuthenticated)
            {
                var user = httpContext.RequestServices.GetService<AuthenticatedUser>();
                user.UserId = httpContext.User.GetUserId();
                user.IsAdmin = httpContext.User.IsInRole(RoleDefaults.DefaultAdminRole);
            }

            await _next(httpContext);
        }
    }
}