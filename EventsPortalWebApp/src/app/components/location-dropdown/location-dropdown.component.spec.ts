import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LocationDropdownComponent } from './location-dropdown.component';

describe('DropdownComponent', () => {
  let component: LocationDropdownComponent;
  let fixture: ComponentFixture<LocationDropdownComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LocationDropdownComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LocationDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
