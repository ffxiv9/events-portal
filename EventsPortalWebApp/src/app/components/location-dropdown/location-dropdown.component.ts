import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { FormGroup } from '@angular/forms';

@Component({
    selector: 'app-locations',
    templateUrl: './location-dropdown.component.html',
    styleUrls: ['./location-dropdown.component.sass']
})
export class LocationDropdownComponent implements OnInit {
    public locations: string[];
    @Input() parentForm = new FormGroup({});
    @Input() requiredError = false;
    @Output() selectedEvent = new EventEmitter<string>();
    selected: string;
    private readonly locationsUrl;

    constructor(private http: HttpClient) {
        this.locationsUrl = `${environment.baseUrl}/api/locations`;
    }

    ngOnInit(): void {
        this.getLocations();
    }

    getLocations(): void {
        this.http.get<string[]>(this.locationsUrl).subscribe(i => {
            this.locations = i;
        });
    }

    reset() {
        this.selected = '';
    }

    valueChange(event): void {
        this.selectedEvent.emit(event);
    }
}
