import {
    AfterViewInit,
    Component,
    DoCheck,
    ElementRef,
    forwardRef,
    Injector,
    Input,
    OnDestroy,
    OnInit,
    ViewChild
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, NgControl } from '@angular/forms';
import * as SimpleMde from 'simplemde';
import { MatFormFieldControl } from '@angular/material/form-field';
import { Subject } from 'rxjs';
import { MarkdownEditorDefaultOptions } from './markdown-editor.options';

@Component({
    selector: 'markdown-editor',
    template: `<textarea #elem></textarea>`,
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => MarkdownEditorComponent),
        multi: true
    }, {
        provide: MatFormFieldControl,
        useExisting: MarkdownEditorComponent
    }]
})
export class MarkdownEditorComponent implements AfterViewInit, OnInit, DoCheck, OnDestroy, ControlValueAccessor, MatFormFieldControl<any> {
    @Input('placeholder') placeholder: string;
    markdownEditor: SimpleMde;
    value: any;
    stateChanges = new Subject<void>();
    id: string;
    ngControl: NgControl;
    focused: boolean;
    empty: boolean;
    shouldLabelFloat: boolean;
    required: boolean;
    disabled: boolean;
    errorState: boolean;
    controlType?: string;
    autofilled?: boolean;
    userAriaDescribedBy?: string;
    @ViewChild('elem') private _elementRef: ElementRef;

    constructor(public injector: Injector) {
    }

    ngDoCheck(): void {
        if (this.ngControl) {
            this.errorState = this.ngControl.invalid && this.ngControl.touched;
            this.stateChanges.next();
        }
    }

    setDescribedByIds(ids: string[]): void {
    }

    onContainerClick(event: MouseEvent): void {
    }

    onChange(_: any) {
    }

    onTouched() {
    }

    ngAfterViewInit(): void {
        this.markdownEditor = new SimpleMde({
            element: this._elementRef.nativeElement,
            placeholder: this.placeholder,
            ...MarkdownEditorDefaultOptions
        });
        this.markdownEditor.codemirror.on('change', () => {
            this.onChange(this.markdownEditor.value());
        });
        this.markdownEditor.codemirror.on('blur', () => {
            this.onTouched();
        });
    }

    ngOnDestroy(): void {
        delete this.markdownEditor;
        this.stateChanges.complete();
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    writeValue(obj: any): void {
        if (this.markdownEditor) {
            this.markdownEditor.value(obj);
        }
    }

    ngOnInit(): void {
        this.ngControl = this.injector.get(NgControl);
        if (this.ngControl != null) this.ngControl.valueAccessor = this;
    }
}
