﻿export const MarkdownEditorDefaultOptions = {
    toolbar: ['bold', 'italic', 'heading', 'strikethrough', '|', 'quote', 'code', '|',
        'unordered-list', 'ordered-list', 'horizontal-rule', '|', 'link', 'image', '|',
        'side-by-side', 'fullscreen']
};
