import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../shared/auth.service';
import { EventsService } from '../../events/shared/services/event.service';
import { Router } from '@angular/router';
import { TimeZoneService } from '../../shared/time-zone.service';
import * as moment from 'moment-timezone';
import { CreateStates } from 'src/app/events/shared/models/CreateStates';
import { CustomIconsService } from '../../shared/custom-icons.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {
    title = 'EventsPortalWebApp';
    selectedTimeZone: string;
    timeZones: string[];

    constructor(
        public authService: AuthService,
        private customIconsService: CustomIconsService,
        private router: Router,
        private timeZoneService: TimeZoneService,
        public eventsService: EventsService
    ) {
        customIconsService.bind();
        this.timeZones = this.timeZoneService.getAllTimeZones()
        .sort( (tz1, tz2) => moment().tz(tz1).utcOffset() - moment().tz(tz2).utcOffset());
    }

    formatTimeZone(timeZone: string): string {
        return `(UTC${moment().tz(timeZone).format('Z')}) ${timeZone}`;
    }

    ngOnInit(): void {
        let zone = this.timeZoneService.getTimeZone();
        this.selectedTimeZone = zone;
        moment.tz.setDefault(zone);
        this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    }

    isLoggedIn(): boolean {
        return this.authService.isLoggedIn();
    }

    logout(): void {
        this.authService.logout();
        this.authService.isLoggedInState = false;
        this.router.navigate(['user/login']);
    }

    valueChange(zone: string): void {
        this.timeZoneService.setTimeZone(zone);
        this.router.navigated = false;
        this.router.navigate([this.router.url]);
    }
}
