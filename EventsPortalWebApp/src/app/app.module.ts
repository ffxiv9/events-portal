import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS} from '@angular/material/form-field';

import {AppComponent} from './components/main/app.component';
import {AppRoutingModule} from './app-routing.module';
import {EventsModule} from './events/events.module';
import {UsersModule} from './users/users.module';
import {AuthInterceptor} from './shared/auth.interceptor';
import {AppMaterialModule} from './shared/material-module';
import {AuthGuard} from './shared/auth-guard.service';
import {CookieService} from 'ngx-cookie-service';
import {AuthService} from './shared/auth.service';
import {MarkdownModule} from 'ngx-markdown';
import {MomentDatePipe} from './shared/MomentDatePipe';
import {TimeZoneService} from './shared/time-zone.service';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {CustomIconsService} from './shared/custom-icons.service';
import { LoginGuard } from './shared/login-guard.service';


@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        EventsModule,
        UsersModule,
        BrowserAnimationsModule,
        HttpClientModule,
        AppMaterialModule,
        MarkdownModule.forRoot()
        // CookieService,
        // MatInputModule,
    ],
    providers: [{
        provide: HTTP_INTERCEPTORS,
        useClass: AuthInterceptor,
        multi: true
    },
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS},
    {provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: { appearance: 'outline' } },
        CookieService,
        AuthGuard,
        LoginGuard,
        AuthService,
        MomentDatePipe,
        TimeZoneService,
        CustomIconsService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
