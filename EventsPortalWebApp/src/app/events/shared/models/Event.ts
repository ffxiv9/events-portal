import { EventVenueType } from './EventVenueType';
import { Speaker } from '../../speakers/shared/Speaker';

export class ContactPerson {
    id: number;
    firstName: string;
    lastName: string;
    position: string;
    email: string;
}

export class Event {
    id: number;
    createdDate: Date;
    startDate: moment.Moment;
    title: string;
    description: string;
    venueType: EventVenueType;
    image: string;
    linkBroadcast: string;
    speakerName: string;
    duration: number;
    location: string;
    canEdit: boolean;
    isUserAttending = false;
    contactPerson: ContactPerson;
    speakers: Speaker[];
}
