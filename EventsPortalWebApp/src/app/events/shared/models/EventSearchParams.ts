export class EventFilter{
    title: string;
    location: string;
    venueType = "onlineAndOffline";
    minDate: moment.Moment;
    maxDate: moment.Moment;
    pageSize = 16;
    pageIndex: number;
}
