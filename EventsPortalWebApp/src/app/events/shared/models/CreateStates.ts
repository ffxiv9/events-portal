export enum CreateStates {
    toCreate = 0,
    inProcess = 1,
    notSavedChanges = 2
}