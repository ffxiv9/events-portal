export enum PermissionType {
    create,
    read,
    update,
    delete
}
