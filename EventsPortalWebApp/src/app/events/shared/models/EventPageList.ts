import { Event } from './Event';
import {Serialize, SerializeProperty,Serializable} from 'ts-serializer';

@Serialize({})
export class EventPageList extends Serializable {
    @SerializeProperty({ map: 'fullCount' })
    totalEvents: number;
    @SerializeProperty({ map: 'list' })
    eventsList: Event[]
}