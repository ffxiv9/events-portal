export enum EventVenueType {
    offline = 1,
    online = 2,
    onlineAndOffline = offline | online
}
