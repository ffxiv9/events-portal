import { EventEditorComponent } from '../event-editor/event-editor.component';
import { Event } from './models/Event';

export function patchValueToEditor(currentEvent: Event, editor: EventEditorComponent): void {
    if (currentEvent == null) {
        return;
    }

    let venueOnline = false;
    let venueOffline = false;
    switch (currentEvent.venueType) {
        case 1: {
            venueOffline = true;
            break;
        }
        case 2: {
            venueOnline = true;
            break;
        }
        case 3: {
            venueOffline = true;
            venueOnline = true;
            break;
        }
    }
    // todo merge this with switch
    if (currentEvent.venueType != null) { delete currentEvent.venueType; }

    let durationTime: string;
    if (currentEvent.duration != null) {
        durationTime = toTimeString(currentEvent.duration);
        delete currentEvent.duration;
    }

    if (currentEvent.image != null) {
        editor.selectedImg = currentEvent.image;
        delete currentEvent.image;
    }

    editor.eventId = currentEvent.id;
    editor.speakers = currentEvent.speakers;
    editor.hasSpeakers = currentEvent.speakers.length > 0;
    editor.eventCreateForm.patchValue({
        ...currentEvent,
        duration: durationTime,
        checkboxGroup: {
            online: venueOnline,
            offline: venueOffline
        }
    });
}

function toTimeString(timeInMinutes: number): string {
    if (!Number.isInteger(timeInMinutes)) {
        return undefined;
    }

    const hours = timeInMinutes / 60 | 0;
    const minutes = timeInMinutes % 60;
    return String(hours).padStart(2, '0') + ':' + String(minutes).padStart(2, '0');
}
