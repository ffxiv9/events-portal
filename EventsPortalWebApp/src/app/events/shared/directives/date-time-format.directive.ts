import { Directive } from '@angular/core';
import {
    NgxMatDateFormats, NGX_MAT_DATE_FORMATS, NgxMatDateAdapter,
  } from '@angular-material-components/datetime-picker';
import { NgxMatMomentAdapter } from '@angular-material-components/moment-adapter';

export const CUSTOM_FORMATS: NgxMatDateFormats = {
    parse: {
        dateInput: 'DD/MM/YYYY, HH:mm'
    },
    display: {
      dateInput: 'DD/MM/YYYY, HH:mm',
      monthYearLabel: 'MMM YYYY',
      dateA11yLabel: 'LL',
      monthYearA11yLabel: 'MMMM YYYY',
    }
  };
@Directive({
    selector: '[appDateTimePickerFormat]',
    providers: [
        { provide: NGX_MAT_DATE_FORMATS, useValue: CUSTOM_FORMATS },
        { provide: NgxMatDateAdapter, useClass: NgxMatMomentAdapter },
      ]
})
export class DatetimePickerFormatDirective {
}
