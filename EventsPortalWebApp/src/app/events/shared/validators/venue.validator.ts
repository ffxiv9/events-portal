import { FormGroup, ValidatorFn } from '@angular/forms';

export function venueValidator(): ValidatorFn {
    return (formGroup: FormGroup): { [key: string]: any } | null => {
        let checked = 0;
        Object.keys(formGroup.controls).forEach(key => {
            const control = formGroup.controls[key];
            if (control.value === true) {
                checked++;
            }
        });

        if (checked < 1) {
            return {
                noTypeSelected: true
            };
        } else {
            return null;
        }
    };
}
