export const attendingOnlineEventMessage = 'You are already attending an event. Don\'t forget to check this page closer to the event start date for broadcast details!';
export const attendingOfflineEventMessage = 'You are already attending an event. Contact the event organizer if you have any questions.';
export const attendingOnlineAndOfflineEventMessage = 'You are already attending an event. Don\'t forget to check this page closer to the event start date for broadcast details. Contact the event organizer if you have any questions.';
export const eventWasNotCreatedOrUpdated = 'Event was not created or updated due to some error'
