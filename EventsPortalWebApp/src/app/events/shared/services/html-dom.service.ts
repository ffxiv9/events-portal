﻿import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';

@Injectable()
export class HtmlDomService {
    private readonly baseUrl: string;
    private readonly linkProps = ['href', 'src'];

    constructor() {
        this.baseUrl = environment.baseUrl;
    }

    public removeIgnoredDomElements(rootElem: any) {
        if (rootElem.getAttribute('export-ignore') != null) {
            return true;
        }
        for (let i = 0; i < rootElem.children.length; i++) {
            if (this.removeIgnoredDomElements(rootElem.children[i])) {
                rootElem.removeChild(rootElem.children[i--]);
            }
        }
    }

    public replaceRelativeToAbsolute(rootElem: any) {
        for (let i = 0; i < rootElem.children.length; i++) {
            this.replaceRelativeToAbsolute(rootElem.children[i]);
        }

        for (let i = 0; i < this.linkProps.length; i++) {
            let link = rootElem.getAttribute(this.linkProps[i]);
            if (link != null) {
                let absoluteUrl = new URL(link, this.baseUrl).href;
                rootElem.setAttribute(this.linkProps[i], absoluteUrl);
            }
        }
    }

    public generateHtmlPage(headElem: any, rootElem: any): string {
        return '<html lang="en">' + headElem.outerHTML + '<body class="mat-typography">' + rootElem.outerHTML + '</body></html>';
    }
}
