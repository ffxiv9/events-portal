import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Event } from '../models/Event';
import { environment } from '../../../../environments/environment';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { EventPageList } from '../models/EventPageList';
import { HttpHeaders } from '@angular/common/http';
import { CreateStates } from '../models/CreateStates';

@Injectable()
export class EventsService {
    private readonly eventUrl;

    constructor(private http: HttpClient) {
        this.eventUrl = `${environment.baseUrl}/api/events`;
    }

    get createState(): CreateStates {
        const parsedValue = parseInt(sessionStorage.getItem('create-state'));
        if(Object.values(CreateStates).includes(parsedValue))
            return parsedValue;
        return 0;
    };

    set createState(state: CreateStates) { sessionStorage.setItem('create-state', state.toString()) };

    getEvents(): Observable<Event[]> {
        return this.http.get<Event[]>(this.eventUrl)
            .pipe(
                catchError(this.handleError<Event[]>('getEvents', []))
            );
    }

    getEventsWithParams(params: string): Observable<EventPageList> {
        return this.http.get<EventPageList>(this.eventUrl + '?' + params)
            .pipe(
                catchError(this.handleError<EventPageList>('getEventsWithParams'))
            );
    }

    createEvent(event: Event): Observable<object> {
        return this.http.post(this.eventUrl, event);
    }

    editEvent(id: number, event: Event): Observable<object> {
        return this.http.put(`${this.eventUrl}/${id}`, event);
    }

    getEvent(id: number): Observable<Event> {
        return this.http.get<Event>(`${this.eventUrl}/${id}`);
    }

    private handleError<T>(operation = 'operation', result?: T): any {
        return (error: any): Observable<T> => {
            console.log(`${operation} failed: ${error.message}`);
            return of(result as T);
        };
    }

    attend(id: number, offset: number): any {
        const myHeaders = new HttpHeaders()
            .set('content-type', 'text/ics');

        return this.http.put(`${this.eventUrl}/attend/${id}/${offset}`, {}, { headers: myHeaders,  responseType: 'blob' })
            .pipe(
                catchError(this.handleError<any>('Attend the event and download the file'))
            );
    }

    unAttend(eventId: number) {
        return this.http.put(`${this.eventUrl}/unattend/${eventId}/`, {})
            .pipe(
                catchError(this.handleError<any>())
            );
    }
}
