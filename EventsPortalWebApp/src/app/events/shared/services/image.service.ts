import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';

@Injectable()
export class ImageService {
    constructor() {
    }

    /**
     * @param id Provide the same number to get the same default image
     */
    public getDefaultImage(id: number): string {
        let imageNumber = id > 0
            ? id % environment.defaultImagesCount
            : Math.floor(Math.random() * Math.floor(environment.defaultImagesCount));

        return `/assets/da-bg/${imageNumber}.svg`;
    }

    public getImageUrl(url: string, width: number = 0, height: number = 0): string {
        let sizeStr = '';
        if (width > 0 && height > 0) {
            sizeStr = `/${width}x${height}`;
        }
        return url.includes(':') ? url : environment.imageServiceUrl + url + sizeStr;
    }
}
