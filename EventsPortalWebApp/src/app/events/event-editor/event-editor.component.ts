import { Component, Input, OnChanges, OnDestroy } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Speaker } from '../speakers/shared/Speaker';
import { SpeakerDialogComponent } from '../speakers/speaker-dilog.component';
import { TimeZoneService } from '../../shared/time-zone.service';
import * as moment from 'moment-timezone';
import { venueValidator } from '../shared/validators/venue.validator';
import { ChillErrorStateMatcher } from '../shared/chill-error-state-matcher';

const maxImageSize = 10 * 1024 * 1024;

@Component({
    selector: 'app-event-editor',
    templateUrl: './event-editor.component.html',
    styleUrls: ['./event-editor.component.sass']
})
export class EventEditorComponent implements OnChanges, OnDestroy {
    @Input('submit') submitCallback: Function;
    @Input('current') currentCallback?: Function;

    eventId: number;
    customMatcher = new ChillErrorStateMatcher();

    urlPattern = '^(?:(?:http(?:s)?|ftp)://)(?:\\S+(?::(?:\\S)*)?@)?(?:(?:[a-z0-9\u00a1-\uffff](?:-)*)*(?:[a-z0-9\u00a1-\uffff])+)(?:\\.(?:[a-z0-9\u00a1-\uffff](?:-)*)*(?:[a-z0-9\u00a1-\uffff])+)*(?:\\.(?:[a-z0-9\u00a1-\uffff]){2,})(?::(?:\\d){2,5})?(?:/(?:\\S)*)?$';
    namePattern = '^[a-zA-ZА-Яа-яЁё]{1,20}[a-zA-ZА-Яа-яЁё \-]{1,20}$';
    durationMask = [/\d/, /\d/, ':', /[0-5]/, /\d/];
    durationModel = '';
    minDate: moment.Moment;
    selectedImg: string | ArrayBuffer;
    hasSpeakers: boolean;
    speakers: Speaker[] = [];
    venueOnline: false;
    venueOffline: false;
    awaitingCreation: boolean;

    eventCreateForm: FormGroup = this.formBuilder.group({
        location: ['', [Validators.required]],
        linkBroadcast: ['', [Validators.pattern(this.urlPattern)]],
        description: ['', [Validators.required]],
        startDate: ['', [Validators.required]],
        duration: ['', [Validators.required]],
        title: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(150)]],
        checkboxGroup: new FormGroup({
            online: new FormControl(false),
            offline: new FormControl(false)
        }, venueValidator())
    });

    constructor(private formBuilder: FormBuilder, public dialog: MatDialog, private timeZoneService: TimeZoneService) {
        const timeZone = this.timeZoneService.getTimeZone();
        moment.tz.setDefault(timeZone);
        this.minDate = moment().add(1, 'hours');
    }

    get isEditMode(): boolean {
        return this.eventId != null && this.eventId !== 0;
    }

    ngOnDestroy(): void {
        if (this.eventCreateForm.controls.linkBroadcast.value === ''
            && this.eventCreateForm.controls.description.value === ''
            && this.eventCreateForm.controls.startDate.value === ''
            && this.eventCreateForm.controls.duration.value === ''
            && this.eventCreateForm.controls.title.value === ''
            && this.eventCreateForm.controls.location.value === ''
            && this.selectedImg === undefined && this.speakers.length === 0) {
            this.clear();
        }
    }

    ngOnChanges(): void {
        if (this.currentCallback != null) {
            this.currentCallback(this.getCurrentEvent());
        }
    }

    onSubmit(): void {
        if (!this.eventCreateForm.valid) {
            this.markFormGroupTouched(this.eventCreateForm);
            return;
        }
        this.submitCallback(this.getCurrentEvent());
    }

    getCurrentEvent(): Event {
        const newEvent = this.eventCreateForm.value;
        if (this.eventCreateForm.controls.duration.value != null) {
            newEvent.duration = this.calcToMinutes(this.eventCreateForm.controls.duration.value);
        } else {
            newEvent.duration = 0;
        }
        newEvent.image = this.selectedImg;
        newEvent.speakers = this.speakers;
        if (!this.venueOnline && this.venueOffline) {
            newEvent.venueType = 1;
        } else if (this.venueOnline && !this.venueOffline) {
            newEvent.venueType = 2;
        } else if (this.venueOnline && this.venueOffline) {
            newEvent.venueType = 3;
        } else {
            newEvent.venueType = 0;
        }
        return newEvent;
    }

    hasError(controlName: string, errorName: string): boolean {
        return this.eventCreateForm.get(controlName).hasError(errorName);
    }

    calcToMinutes(time: string): number {
        const hoursInMinutes = Number.parseInt(time.slice(0, 2)) * 60;
        const minutes = Number.parseInt(time.slice(3, 5));
        return hoursInMinutes + minutes;
    }

    onImgChanged(event): void {
        this.uploadImage(event.target.files[0]);
    }

    openDialog(speaker: Speaker = null): void {
        if (!speaker) {
            speaker = new Speaker();
        }
        const dialogRef = this.dialog.open(SpeakerDialogComponent, {
            width: '850px',
            data: speaker
        });

        dialogRef.afterClosed().subscribe(result => {
            if (this.speakers.indexOf(speaker) < 0 && result) {
                this.speakers.push(result as Speaker);
            }
        });
    }

    deleteSpeaker = (speaker: Speaker): void => {
        this.speakers.splice(this.speakers.indexOf(speaker), 1);
    };

    editSpeaker(speaker: Speaker): void {
        this.openDialog(speaker);
    }

    clear(): void {
        if (this.currentCallback != null) {
            this.currentCallback(null);
        }
    }

    uploadImage(imageFile: any) {
        const reader = new FileReader();
        reader.readAsDataURL(imageFile);
        if (!this.canImageBeUploaded(imageFile)) {
            return;
        }
        reader.onload = (_event) => {
            this.selectedImg = reader.result;
        };
    }

    private canImageBeUploaded(file: any): boolean {
        return file != null && file.size <= maxImageSize;
    }

    private markFormGroupTouched(eventCreateForm: FormGroup): void {
        (Object as any).values(eventCreateForm.controls).forEach((control: FormGroup) => {
            control.markAsTouched();
            if (control.controls) {
                this.markFormGroupTouched(control);
            }
        });
    }
}
