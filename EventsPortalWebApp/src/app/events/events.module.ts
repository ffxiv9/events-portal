import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventsRoutingModule } from './events-routing.module';
import { EventsListComponent } from './events-list/events-list.component';
import { EventDetailComponent } from './event-detail/event-detail.component';
import { AppMaterialModule } from '../shared/material-module';
import { EventsService } from './shared/services/event.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EventsSearchComponent } from './events-search/events-search.component';
import { ImagePreloadDirective } from './image-preload.directive';
import { TextMaskModule } from 'angular2-text-mask';
import { MarkdownModule } from 'ngx-markdown';
import { MarkdownEditorComponent } from '../components/markdown/markdown-editor.component';
import { DatetimePickerFormatDirective } from './shared/directives/date-time-format.directive';
import { NgxTrimDirectiveModule } from 'ngx-trim-directive';
import { CreateEventComponent } from './create-event/create-event.component';
import { EditEventComponent } from './edit-event/edit-event.component';
import { EventEditorComponent } from './event-editor/event-editor.component';
import { SpeakerDialogComponent } from './speakers/speaker-dilog.component';
import { MomentDatePipe } from '../shared/MomentDatePipe';
import { SpeakerComponent } from './speakers/speaker.component';
import { LocationDropdownComponent } from '../components/location-dropdown/location-dropdown.component';
import { HtmlDomService } from './shared/services/html-dom.service';
import { ImageService } from './shared/services/image.service';
import { PaginatorDirective } from './shared/directives/pagination.directive';
import { IvyCarouselModule } from 'angular-responsive-carousel';
import { DragDropDirective } from './shared/directives/drag-drop.directive';

@NgModule({
    declarations: [
        CreateEventComponent,
        EditEventComponent,
        EventEditorComponent,
        EventDetailComponent,
        EventsListComponent,
        EventsSearchComponent,
        ImagePreloadDirective,
        MarkdownEditorComponent,
        DragDropDirective,
        DatetimePickerFormatDirective,
        PaginatorDirective,
        LocationDropdownComponent,
        SpeakerDialogComponent,
        MomentDatePipe,
        SpeakerComponent
    ],
    imports: [
        CommonModule,
        EventsRoutingModule,
        AppMaterialModule,
        FormsModule,
        TextMaskModule,
        ReactiveFormsModule,
        NgxTrimDirectiveModule,
        MarkdownModule,
        IvyCarouselModule
    ],
    entryComponents: [EventEditorComponent, SpeakerDialogComponent],
    providers: [EventsService, HtmlDomService, ImageService]
})
export class EventsModule {
}
