import {AfterViewInit, Component, enableProdMode, OnInit, ViewChild} from '@angular/core';
import { EventsService } from '../shared/services/event.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Event } from '../shared/models/Event';
import * as Constants from '../shared/constants';
import { EventEditorComponent } from '../event-editor/event-editor.component';
import * as moment from 'moment-timezone';
import { patchValueToEditor } from '../shared/patch-value-to-editor.function';

@Component({
    selector: 'app-edit-event',
    template: '<mat-error *ngIf="error">{{error}}</mat-error>'
        + '<app-event-editor [submit]="editEvent.bind(this)"></app-event-editor>'
})
export class EditEventComponent implements OnInit, AfterViewInit {
    currentEvent: Event;
    error: string;
    @ViewChild(EventEditorComponent) editor: EventEditorComponent;

    constructor(private eventService: EventsService, private router: Router, private route: ActivatedRoute) {
        this.currentEvent = this.route.snapshot.data.event;
    }

    ngAfterViewInit(): void {
        setTimeout(() => patchValueToEditor(this.currentEvent, this.editor));
    }

    ngOnInit(): void {
    }


    editEvent(event: Event): void {
        this.editor.awaitingCreation = true;
        this.eventService.editEvent(this.currentEvent.id, event).subscribe(
            () => this.router.navigate(['event/' + this.currentEvent.id]),
            error => {
                this.editor.awaitingCreation = false;
                console.log(error);
                this.error = Constants.eventWasNotCreatedOrUpdated;
            }
        );
    }
}
