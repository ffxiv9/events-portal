import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { EventsService } from '../shared/services/event.service';
import { catchError, map } from 'rxjs/operators';

@Injectable()
export class EditEventResolver implements Resolve<any> {
    constructor(private eventsService: EventsService, private router: Router) {
    }

    resolve(route: ActivatedRouteSnapshot): Observable<any> {
        let param = route.paramMap.get('id');
        let id = parseInt(param);
        if (id > 0 && param.toString().length == id.toString().length) {
            return this.eventsService.getEvent(id).pipe(
                map(e => {
                    if (!e.canEdit) {
                        throw new Error('User does not have access privileges');
                    }

                    e.id = id;
                    return e;
                }),
                catchError(e => this.router.navigate(['/event/' + id]))
            );
        }
        this.router.navigate(['/']);
    }
}
