import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Event } from '../shared/models/Event';
import { EventsService } from '../shared/services/event.service';
import { Router } from '@angular/router';
import * as Constants from '../shared/constants';
import { EventEditorComponent } from '../event-editor/event-editor.component';
import { CreateStates } from '../shared/models/CreateStates';
import { patchValueToEditor } from '../shared/patch-value-to-editor.function';

@Component({
    selector: 'app-create-event',
    template: '<mat-error *ngIf="error">{{error}}</mat-error>'
        + '<app-event-editor [submit]="createEvent.bind(this)" [current]="saveToStorage.bind(this)"></app-event-editor>'
})
export class CreateEventComponent implements OnInit, AfterViewInit, OnDestroy {
    error: string;
    currentEvent: Event;
    @ViewChild(EventEditorComponent) editor: EventEditorComponent;
    constructor(private eventService: EventsService, private router: Router) {
        this.loadFromStorage();
    }

    ngOnDestroy(): void {
        if (this.eventService.createState === CreateStates.inProcess) {
            this.eventService.createState = CreateStates.notSavedChanges;
        }
    }

    ngOnInit(): void {
        this.eventService.createState = CreateStates.inProcess;
    }

    ngAfterViewInit(): void {
        setTimeout(() => patchValueToEditor(this.currentEvent, this.editor), 1000);
    }

    createEvent(newEvent: Event): void {
        this.editor.awaitingCreation = true;
        this.eventService.createEvent(newEvent)
            .subscribe(
                (e) => {
                    this.router.navigate(['/']);
                    sessionStorage.removeItem('creating-event');
                    this.eventService.createState = CreateStates.toCreate;
                },
                error => {
                    this.editor.awaitingCreation = false;
                    console.log(error);
                    this.error = Constants.eventWasNotCreatedOrUpdated;
                }
            );
    }

    saveToStorage(newEvent: Event): void {
        if (newEvent == null) {
            this.eventService.createState = CreateStates.toCreate;
            sessionStorage.removeItem('creating-event');
        } else {
            sessionStorage.setItem('creating-event', JSON.stringify(newEvent));
        }
    }

    loadFromStorage(): void {
        const jsonObj = sessionStorage.getItem('creating-event');
        if (jsonObj !== '' && jsonObj != null) {
            this.currentEvent = JSON.parse(jsonObj, this.dataReviver);
        }
    }

    // Need to deserealize data string to data object
    // Otherwise the date will remain a string, and you won't be able to call the date object methods
    dataReviver(key, value) {
        const dateFormat = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z$/;
        if (typeof value === 'string' && dateFormat.test(value)) {
            return new Date(value);
        }
        return value;
    }

    toTimeString(timeInMinutes: number): string {
        if (!Number.isInteger(timeInMinutes)) {
            return '00:00';
        }

        const hours = timeInMinutes / 60 | 0;
        const minutes = timeInMinutes % 60;
        return String(hours).padStart(2, '0') + ':' + String(minutes).padStart(2, '0');
    }
}
