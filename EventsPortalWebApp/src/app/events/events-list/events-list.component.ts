import {Component, Input, OnInit} from '@angular/core';
import {environment} from 'src/environments/environment';

@Component({
    selector: 'events-list',
    templateUrl: 'events-list.component.html',
    styleUrls: ['events-list.component.sass']
})

export class EventsListComponent implements OnInit {
    @Input('events') events: Event[];
    environment = environment;

    constructor() {
    }

    ngOnInit(): void {
    }
}
