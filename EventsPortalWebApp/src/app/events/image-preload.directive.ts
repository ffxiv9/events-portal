﻿import { Directive, Input } from '@angular/core';
import { ImageService } from './shared/services/image.service';

@Directive({
    selector: 'img[appDefaultImage]',
    host: {
        '(error)': 'useDefaultImage()',
        '[src]': 'src',
        '[style]': 'style'
    }
})
export class ImagePreloadDirective {
    @Input('id')
    id: number;
    @Input('img_width')
    width: number;
    @Input('img_height')
    height: number;
    style: string;

    constructor(private imageService: ImageService) {
    }

    private _src = '';

    @Input()
    get src(): string {
        return this._src;
    }

    set src(src: string) {
        if (src != null && src.length > 0) {
            this._src = this.imageService.getImageUrl(src, this.width, this.height);
            return;
        }

        this.useDefaultImage();
    }

    useDefaultImage(): void {
        this.style = 'object-fit: cover;';
        this._src = this.imageService.getDefaultImage(this.id);
    }
}
