export class Speaker {
    id: number;
    name: string;
    image: string | ArrayBuffer;
    position: string;
    description: string;
}
