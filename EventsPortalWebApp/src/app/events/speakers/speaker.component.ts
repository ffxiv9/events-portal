import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Speaker } from '../speakers/shared/Speaker';
import { environment } from '../../../environments/environment';

@Component({
    selector: 'app-speaker',
    templateUrl: './speaker.component.html',
    styleUrls: ['./speaker.component.sass']
})
export class SpeakerComponent {
    @Input() isEditable = true;
    @Input() speaker: Speaker;
    @Output() deleteSpeaker = new EventEmitter<Speaker>();
    @Output() editSpeaker = new EventEmitter<Speaker>();

    delete(): void {
        this.deleteSpeaker.next(this.speaker);
    }
    edit(): void {
        this.editSpeaker.next(this.speaker);
    }
}
