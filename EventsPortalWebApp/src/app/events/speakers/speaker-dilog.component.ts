import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Speaker } from './shared/Speaker';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    selector: 'app-speaker-dialog',
    templateUrl: 'speaker-dialog.html',
    styleUrls: ['./speaker-dialog.component.sass']
})
export class SpeakerDialogComponent implements OnInit {

    constructor(
        public dialogRef: MatDialogRef<SpeakerDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: Speaker,
        private formBuilder: FormBuilder) {
        dialogRef.disableClose = true;
    }

    speakerForm = this.formBuilder.group({
        name: ['', [Validators.required]],
        position: ['', [Validators.required]],
        description: ['description', [Validators.required]]
    });

    ngOnInit(): void {
        this.speakerForm.patchValue({
            name: this.data.name,
            position: this.data.position,
            description: this.data.description
        });
    }

    cancel(): void {
        this.dialogRef.close();
    }

    save(): void {
        if (!this.speakerForm.valid) {
            this.markFormGroupTouched(this.speakerForm);
            return;
        }
        this.dialogRef.close(this.data);
    }

    uploadImage(imageFile: any) {
        const reader = new FileReader();
        reader.readAsDataURL(imageFile);
        reader.onload = (_event) => {
            this.data.image = reader.result;
        };
    }

    onImgChanged(event): void {
        this.uploadImage(event.target.files[0]);
    }

    private markFormGroupTouched(form: FormGroup): void {
        (Object as any).values(form.controls).forEach((control: FormGroup) => {
            control.markAsTouched();

            if (control.controls) {
                this.markFormGroupTouched(control);
            }
        });
    }
}
