import { HttpClient, HttpParams } from '@angular/common/http';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { EventPageList } from '../shared/models/EventPageList';
import { Event } from '../shared/models/Event';
import { EventsService } from '../shared/services/event.service';
import { EventFilter } from '../shared/models/EventSearchParams';
import { TimeZoneService } from '../../shared/time-zone.service';
import * as moment from 'moment-timezone';
import { LocationDropdownComponent } from '../../components/location-dropdown/location-dropdown.component';

@Component({
    selector: 'events-search',
    templateUrl: './events-search.component.html',
    styleUrls: ['./events-search.component.sass'],
})
export class EventsSearchComponent implements OnInit, AfterViewInit {
    @ViewChild(LocationDropdownComponent) locationDropdownComponent: LocationDropdownComponent;
    events: Event[] = [];
    eventFilter: EventFilter = new EventFilter();
    count: number;
    loaded: boolean;
    images = [
        {path: 'https://99px.ru/sstorage/53/2020/04/mid_302207_494268.jpg'},
        {path: 'https://wallpaperhunt.net/save/wallpaper?f=24577-4k-minimalist-wallpaper-3840x2160-158074280832820.jpg'},
        {path: 'https://images3.alphacoders.com/954/thumb-1920-954241.jpg'},
        {path: 'https://images.hdqwalls.com/download/minimalist-landscape-to-1920x1080.jpg'},
        {path: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQsPOySB1HaYW4QshPjcc2nbZ0133lqqtcpNA&usqp=CAU'},
        {path: 'https://cdn.statically.io/img/images.wallpapersden.com/image/download/minimalist-landscape-painting_67136_1920x1080.jpg'},
        {path: 'https://images.hdqwalls.com/download/eagle-landscape-mountains-minimalist-ez-1920x1080.jpg'}
    ];

    constructor(private http: HttpClient, private eventsService: EventsService) {
    }

    //todo should be replaced. just returns a venue type string in a user friendly way (capitalize first letter)
    get eventVenueTypeString() {
        let replace = this.eventFilter.venueType.replace('And', ' And ');
        let chars = replace.split('');
        for (let i = 0; i < chars.length; i++) {
            if (i == 0 || chars[i - 1] == ' ')
                chars[i] = chars[i].toUpperCase();
        }
        return chars.join('');
    }

    ngOnInit(): void {
    }

    ngAfterViewInit(): void {
        this.loadSessionState();
        this.getEvents();
    }

    getEvents(): void {
        let eventFilter = {...this.eventFilter};
        eventFilter.maxDate = eventFilter.maxDate?.clone().add(1, 'days').subtract(1, 'seconds');
        const params = this.getParams(eventFilter);
        this.eventsService.getEventsWithParams(params)
            .subscribe(this.handleGetResponse.bind(this));
        this.saveSessionState();
    }

    getParams(eventFilter: EventFilter = null): string {
        if (!eventFilter) eventFilter = this.eventFilter;
        const today = moment();
        today.set({hour: 0, minute: 0, second: 0, millisecond: 0});
        const params = new HttpParams()
            .set('title', eventFilter.title ?? '')
            .set('location', eventFilter.location ?? '')
            .set('venueType', eventFilter.venueType ?? '')
            .set('minDate', eventFilter.minDate?.toISOString() ?? today.toISOString())
            .set('maxDate', eventFilter.maxDate?.toISOString() ?? '')
            .set('pageSize', eventFilter.pageSize?.toString() ?? '')
            .set('pageNumber', ((eventFilter.pageIndex ?? 0) + 1).toString());
        return params.toString();
    }

    saveSessionState(): void {
        const params = this.getParams();
        sessionStorage.setItem('events-page-params', params);
    }

    handleGetResponse(obj: any): void {
        const eventsPage = new EventPageList();
        eventsPage.deserialize(obj);
        this.events = eventsPage.eventsList;
        this.count = eventsPage.totalEvents;
        this.loaded = true;
    }

    selectLocation(event): void {
        this.eventFilter.location = event;
    }

    handlePage(e: any): void {
        this.eventFilter.pageIndex = e.pageIndex;
        this.eventFilter.pageSize = e.pageSize;
        this.loaded = false;
        this.getEvents();
    }

    search(): void {
        this.eventFilter.pageIndex = 0;
        this.getEvents();
    }

    loadSessionState(): void {
        const params = sessionStorage.getItem('events-page-params');
        if (params !== '' && params != null) {
            let par = new HttpParams({fromString: params});
            this.eventFilter.title = par.get('title');
            this.eventFilter.location = par.get('location');
            this.eventFilter.venueType = par.get('venueType');
            this.eventFilter.minDate = moment.utc(par.get('minDate'));
            this.eventFilter.maxDate = moment.utc(par.get('maxDate'));
            this.eventFilter.pageSize = +par.get('pageSize');
            this.eventFilter.pageIndex = 0;
        }
        if (!this.eventFilter.maxDate || this.eventFilter.maxDate.isValid() == false) this.eventFilter.minDate = null;
    }

    reset(): void {
        this.eventFilter = new EventFilter();
        this.locationDropdownComponent.reset();
        this.getEvents();
    }

    resetDate(): void {
        this.eventFilter.minDate = null;
        this.eventFilter.maxDate = null;
    }
}
