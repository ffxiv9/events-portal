﻿import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Event } from '../shared/models/Event';
import { EventsService } from '../shared/services/event.service';
import * as fileSaver from 'file-saver';
import { EventVenueType } from '../shared/models/EventVenueType';
import * as Constants from '../shared/constants';
import { environment } from 'src/environments/environment';
import * as moment from 'moment-timezone';
import { HtmlDomService } from '../shared/services/html-dom.service';

@Component({
    selector: 'app-event-detail',
    templateUrl: './event-detail.component.html',
    styleUrls: ['./event-detail.component.sass']
})

export class EventDetailComponent implements OnInit {
    event: Event;
    eventId: number;
    MAX_TITLE_LENGTH = 30;
    constants: any;
    environment = environment;

    constructor(private route: ActivatedRoute, private eventService: EventsService, private htmlDomService: HtmlDomService) {
        this.route.params.subscribe(params => {
            this.eventId = params['id'];
            this.getEventById(this.eventId);
        });
        this.constants = Constants;
    }

    get eventVenueTypeString(): string {
        return EventVenueType[this.event.venueType].toString().replace('And', ' & ');
    }

    ngOnInit(): void {
    }

    getEventById(id: number): void {
        this.eventService.getEvent(id).subscribe(e => this.event = e);
    }

    calcEndDate(): moment.Moment {
        return moment.utc(this.event.startDate).clone().add(this.event.duration, 'minutes');
    }

    export() {
        const eventRootElem = document.getElementsByClassName('da-layout-wrapper')[0].cloneNode(true) as HTMLElement;
        const headElem = document.getElementsByTagName('head')[0].cloneNode(true) as HTMLElement;

        this.htmlDomService.removeIgnoredDomElements(eventRootElem);
        this.htmlDomService.replaceRelativeToAbsolute(eventRootElem);
        this.htmlDomService.replaceRelativeToAbsolute(headElem);

        const html = this.htmlDomService.generateHtmlPage(headElem, eventRootElem);
        const blob: any = new Blob([html], {type: 'text/html'});
        const filename = this.getFileName('.html');

        fileSaver.saveAs(blob, filename);
    }

    attend(): void {
        const fileName = this.getFileName('.ics');
        this.eventService.attend(this.eventId, moment().utcOffset())
            .subscribe((response: BlobPart) => {
                const blob: any = new Blob([response]);
                fileSaver.saveAs(blob, fileName);
                this.event.isUserAttending = true;
            });
    }

    unAttend(): void {
        this.eventService.unAttend(this.eventId)
            .subscribe(() => {
                this.event.isUserAttending = false;
            });
    }

    private getFileName(extension: string): string {
        let title = this.event.title.replace(' ', '_');
        if (title.length > this.MAX_TITLE_LENGTH) {
            title = title.slice(0, this.MAX_TITLE_LENGTH) + '...';
        }
        const dd = moment(this.event.startDate).format('DD');
        const mm = moment(this.event.startDate).format('MM');
        return `${dd}_${mm}_${title}${extension}`;
    }
}
