import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
    private readonly loginRedirectUrl = 'user/login';

    constructor(private authService: AuthService, private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        if (this.authService.isLoggedIn()) {
            if (route.data.roles && route.data.roles.indexOf(this.authService.getRole()) === -1) {
                this.router.navigate([this.loginRedirectUrl]);
                return false;
            }
            return true;
        }
        this.router.navigate([this.loginRedirectUrl], {queryParams: {returnUrl: state.url}});
        return false;
    }
}
