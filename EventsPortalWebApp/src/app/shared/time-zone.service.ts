import { Injectable } from '@angular/core';
import * as moment from 'moment-timezone';

@Injectable()
export class TimeZoneService {

    constructor() {
    }

    public setTimeZone(timeZone: string): void {
        localStorage.setItem('timeZone', timeZone);
    }

    public setBrowserTimeZone(): void {
        const timeZone = moment.tz.guess(true);
        localStorage.setItem('timeZone', timeZone);
    }

    public getTimeZone(): string {
        let timeZone = localStorage.getItem('timeZone');
        if (!moment.tz.names().some(x => x === timeZone)) {
            timeZone = moment.tz.guess(true);
        }
        return timeZone;
    }

    public getAllTimeZones(): string[] {
        return moment.tz.names();
    }
}
