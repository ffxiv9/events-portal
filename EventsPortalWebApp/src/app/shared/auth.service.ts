import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';
import * as  moment from 'moment';
import jwt_decode from 'jwt-decode';
import { catchError } from 'rxjs/operators';

@Injectable()
export class AuthService {
    private baseUrl: string;
    private tokenKey: string;
    public isLoggedInState: boolean;

    constructor(
        private http: HttpClient,
        private cookieService: CookieService,
        private router: Router
    ) {
        this.baseUrl = environment.baseUrl;
        this.tokenKey = environment.tokenKey;
    }

    public login(viewModel: any): Observable<object> {
        return this.getToken(viewModel)
            .pipe(
                catchError(err => {
                    return of({status: err.status});
                })
            );
    }

    public finalizeLogin(token: string): void {
        this.cookieService.set(this.tokenKey, token, {path: '/'});
        this.router.navigate(['/']);
    }

    public logout(): void {
        localStorage.clear();
        sessionStorage.clear();
        this.cookieService.delete(this.tokenKey, '/');
    }

    public isLoggedIn(): boolean {
        this.isLoggedInState = moment().isBefore(this.getExpiresAt()) || false;
        if (!this.isLoggedInState) {
            this.logout();
        }
        return this.isLoggedInState;
    }

    public getUsername(): string {
        const token = this.cookieService.get(this.tokenKey);
        return token ? jwt_decode(token).sub : null;
    }

    public getRole(): boolean {
        const token = this.cookieService.get(this.tokenKey);
        return token ? jwt_decode(token).role : null;
    }

    private getExpiresAt(): Date {
        const token = this.cookieService.get(this.tokenKey);
        if (token) {
            const payload = jwt_decode(token);
            const expirationDate = payload.exp * 1000;
            return new Date(expirationDate);
        }
        return null;
    }

    private getToken(viewModel: any): Observable<object> {
        return this.http.post(this.baseUrl + '/api/users/login', viewModel);
    }
}
