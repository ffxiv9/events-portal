import {Injectable} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';
import {MatIconRegistry} from '@angular/material/icon';


@Injectable()
export class CustomIconsService {

    constructor(
        private iconRegistry: MatIconRegistry,
        private sanitizer: DomSanitizer) {
    }

    public bind(): void {
        this.iconRegistry.addSvgIcon(
            'da_access_time',
            this.sanitizer.bypassSecurityTrustResourceUrl('assets/access_time.svg'));

        this.iconRegistry.addSvgIcon(
            'can_edit',
            this.sanitizer.bypassSecurityTrustResourceUrl('assets/can_edit.svg'));

        this.iconRegistry.addSvgIcon(
            'is_attending',
            this.sanitizer.bypassSecurityTrustResourceUrl('assets/is_attending.svg'));

        this.iconRegistry.addSvgIcon(
            'share_event',
            this.sanitizer.bypassSecurityTrustResourceUrl('assets/share.svg'));
    }
}
