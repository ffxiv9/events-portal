import { Injectable } from '@angular/core';
import { HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    constructor(private authService: AuthService, private router: Router) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler) {
        request = request.clone({
            withCredentials: true
        });
        return next.handle(request).pipe(catchError(err => {
            if (err.status == 401) {
                this.authService.logout();
                location.reload();
            }

            if (err.status == 403) {
                this.router.navigate(['/']);
            }
            const error = err.error.message || err.statusText;
            return throwError(error);
        }));
    }
}
