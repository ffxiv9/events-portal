import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';
import * as moment from 'moment-timezone';
import { TimeZoneService } from './time-zone.service';

@Pipe({
    name: 'momentDate'
})
export class MomentDatePipe extends DatePipe implements PipeTransform {

    constructor(public timeZoneService: TimeZoneService) {
        super('en-US');
    }

    private getTimeZone(): string {
        return this.timeZoneService.getTimeZone();
    }

    transform(
        value: string | Date,
        format: string,
        timezone: string = this.getTimeZone()
    ): string {
        const timezoneOffset = moment.utc(value).tz(timezone).format(format);
        return timezoneOffset;
    }
}
