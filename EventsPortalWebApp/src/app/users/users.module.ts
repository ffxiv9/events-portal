import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppMaterialModule } from '../shared/material-module';
import { UsersRoutingModule } from './users-routing.module';
import { LoginUserComponent } from './login-user/login-user.component';

@NgModule({
    declarations: [LoginUserComponent],
    imports: [
        CommonModule,
        UsersRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        AppMaterialModule
    ],
    providers: []
})
export class UsersModule { }
