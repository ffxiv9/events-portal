import {Component, HostListener} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../shared/auth.service';

@Component({
    selector: 'app-login',
    templateUrl: './login-user.component.html',
    styleUrls: ['./login-user.component.sass']
})
export class LoginUserComponent {
    hide = true;
    loginForm: FormGroup;
    awaitingLogin = false;
    status = 200;
    USERNAME_MIN_LENGTH = 9;
    USERNAME_MAX_LENGTH = 35;
    PASSWORD_MIN_LENGTH = 6;
    PASSWORD_MAX_LENGTH = 30;

    constructor(
        private formBuilder: FormBuilder,
        private authService: AuthService
    ) {
        this.loginForm = this.formBuilder.group({
            username: ['', [
                Validators.required,
                Validators.minLength(this.USERNAME_MIN_LENGTH),
                Validators.maxLength(this.USERNAME_MAX_LENGTH)]
            ],
            password: ['', [
                Validators.required,
                Validators.minLength(this.PASSWORD_MIN_LENGTH),
                Validators.maxLength(this.PASSWORD_MAX_LENGTH)]
            ]
        });
    }

    hasError(controlName: string, errorName: string): boolean {
        return this.loginForm.get(controlName).hasError(errorName);
    }

    public markFormGroupTouched(loginForm: FormGroup): void {
        (Object as any).values(loginForm.controls).forEach(control => {
            control.markAsTouched();

            if (control.controls) {
                this.markFormGroupTouched(control);
            }
        });
    }

    @HostListener('window:keydown', ['$event'])
    onKeyDown(event): void {
        if (event.key === 'Enter') {
            this.login();
        }
    }

    login(): void {
        this.status = 200;
        if (this.loginForm.invalid) {
            this.markFormGroupTouched(this.loginForm);
            return;
        }
        this.awaitingLogin = true;
        const val = this.loginForm.value;
        this.authService.login(val)
            .subscribe(
                x => {
                    if (x['status']) {
                        this.status = x['status'];
                        this.markFormGroupTouched(this.loginForm);
                    }
                    if (x['token']) {
                        this.authService.finalizeLogin(x['token']);
                    }

                    this.awaitingLogin = false;
                }
        );
    }
}
