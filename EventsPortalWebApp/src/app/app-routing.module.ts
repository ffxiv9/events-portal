import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginUserComponent } from './users/login-user/login-user.component';
import { EventsSearchComponent } from './events/events-search/events-search.component';
import { EventDetailComponent } from './events/event-detail/event-detail.component';
import { AuthGuard } from './shared/auth-guard.service';
import { CreateEventComponent } from './events/create-event/create-event.component';
import { EditEventComponent } from './events/edit-event/edit-event.component';
import { EditEventResolver } from './events/edit-event/edit-event.resolver';
import { LoginGuard } from './shared/login-guard.service';

const routes: Routes = [
    {path: 'event/create', component: CreateEventComponent, canActivate: [AuthGuard]},
    {
        path: 'event/edit/:id',
        component: EditEventComponent,
        canActivate: [AuthGuard],
        resolve: {event: EditEventResolver}
    },
    {path: 'event/:id', component: EventDetailComponent, canActivate: [AuthGuard]},
    {path: 'user/login', component: LoginUserComponent, canActivate: [LoginGuard]},
    {path: '**', component: EventsSearchComponent, canActivate: [AuthGuard]}
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {anchorScrolling: 'enabled'})],
    exports: [RouterModule],
    providers: [EditEventResolver]
})
export class AppRoutingModule {
}
