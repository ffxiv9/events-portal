export const environment = {
    production: true,
    baseUrl: 'https://events-portal.azurewebsites.net/backend',
    defaultImagesCount: 17,
    imageServiceUrl: 'https://python-events-portal.azurewebsites.net/api/image/',
    tokenKey: 'SESSIONID'
};
