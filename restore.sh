#!/bin/bash

pwd=$PWD

dotnetApp="$pwd/EventsPortal.Service/EventsPortal.WebApi/"
angularApp="$pwd/EventsPortalWebApp"
djangoApp="$pwd/integration_service"

(cd $dotnetApp; dotnet restore) & 
(cd $angularApp; npm install) & 
(cd $djangoApp; pip install -r requirements.txt)
