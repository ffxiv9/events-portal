#!/bin/bash

pwd=$PWD

dotnetApp="$pwd/EventsPortal.Service/EventsPortal.WebApi/"
angularApp="$pwd/EventsPortalWebApp"
djangoApp="$pwd/integration_service"

(cd $dotnetApp; dotnet run) &  
(cd $djangoApp; py manage.py runserver)
